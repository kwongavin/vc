import AVFoundation
import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        guard let windowScene = scene as? UIWindowScene else {
            return
        }

        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
        } catch {
            print("Error setting audio session category:", error)
        }

        window = .init(windowScene: windowScene)
        window?.rootViewController = DarkHostingController(rootView: ViewManager())
        window?.makeKeyAndVisible()
    }
}
