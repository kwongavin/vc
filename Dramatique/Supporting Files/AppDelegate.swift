import UIKit
//import IQKeyboardManagerSwift
import Firebase

@main class AppDelegate: UIResponder, UIApplicationDelegate {
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        DatabaseHelper.shared.prepare()
        FirebaseApp.configure()

        UITextField.appearance().keyboardAppearance = UIKeyboardAppearance.dark
        return true
    }

    func application(
        _ application: UIApplication,
        configurationForConnecting connectingSceneSession: UISceneSession,
        options: UIScene.ConnectionOptions
    ) -> UISceneConfiguration {
        .init(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        UserDefaults.standard.setValue(AppManager.shared.showTheatreMode, forKey: "showTheatreMode")
        print("test \(AppManager.shared.showTheatreMode)")
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {

        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return AppManager.shared.isFullPreviewEnabled  ? .allButUpsideDown : .portrait
    }

}
