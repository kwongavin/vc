import SwiftUI

final class DarkHostingController<ContentView> : UIHostingController<ContentView> where ContentView : View {
    override dynamic public var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
}
