import SwiftUI

// stackoverflow.com/a/56516219/3151675
struct VideoPickerController: UIViewControllerRepresentable {
    @Environment(\.presentationMode) var presentationMode

    let sourceType: UIImagePickerController.SourceType
    let callback: (URL?) -> Void

    final class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        @Binding private var presentationMode: PresentationMode
        private let sourceType: UIImagePickerController.SourceType
        private let callback: (URL?) -> Void

        init(presentationMode: Binding<PresentationMode>, sourceType: UIImagePickerController.SourceType, callback: @escaping (URL?) -> Void) {
            _presentationMode = presentationMode
            self.sourceType = sourceType
            self.callback = callback
        }

        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
            guard let url = info[.mediaURL] as? URL else {
                callback(nil)

                return
            }

            callback(url)

            presentationMode.dismiss()
        }

        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            presentationMode.dismiss()
        }
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(presentationMode: presentationMode, sourceType: sourceType, callback: callback)
    }

    func makeUIViewController(context: UIViewControllerRepresentableContext<VideoPickerController>) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.overrideUserInterfaceStyle = .dark
        picker.sourceType = sourceType
        picker.allowsEditing = true
        picker.mediaTypes = ["public.movie"]
        picker.videoQuality = .typeHigh
        picker.delegate = context.coordinator
        return picker
    }

    func updateUIViewController(_ uiViewController: UIImagePickerController, context: UIViewControllerRepresentableContext<VideoPickerController>) {

    }
}
