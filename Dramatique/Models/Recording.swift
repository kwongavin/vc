import Foundation

final class Recording: Hashable {
    let uuid = UUID().uuidString

    var start: Double
    var end: Double?
    var isNew = true
    
    var duration: Double {
        (end ?? 0) - start
    }

    init(start: Double, end: Double? = nil) {
        self.start = start
        self.end = end
    }

    static func == (lhs: Recording, rhs: Recording) -> Bool {
        lhs.uuid == rhs.uuid
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(uuid)
    }
}
