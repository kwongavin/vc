import AVFoundation

final class CustomQueuePlayer: AVQueuePlayer {
    private var volumeTimer: Timer?

    func setVolume(_ targetVolume: Float, duration: Float, completion: (() -> Void)? = nil) {
        setVolume(targetVolume, duration: duration, timer: &volumeTimer, completion: completion)
    }
}
