enum UndoableAction: Hashable {
    case recordedSamples(_ recordedSamples: [[Recording]])
    case selectedSamples(_ selectedSamples: [Sample?], _ changeIndex: Int)
}
