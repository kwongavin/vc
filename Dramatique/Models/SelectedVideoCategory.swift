//
//  SelectedVideoCategory.swift
//  Dramatique
//
//  Created by Difeng Chen on 6/5/22.
//

import Foundation

enum SelectedVideoCategory: Int, CaseIterable {
    case category1 = 2
    case category2 = 1
    case category3 = 0
    case all = 3
    case category1_2 = 4
    case category2_3 = 5
    case category3_1 = 6


    var name: String {
        switch self {
        case .category1:  return "category1"
        case .category2:  return "category2"
        case .category3:  return "category3"
        case .all:        return "all"
        case .category1_2:return "category1_2"
        case .category2_3:return "category2_3"
        case .category3_1:return "category3_1"
        }
    }
}
