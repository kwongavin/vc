import SwiftUI

final class Sample: Hashable, Identifiable {
    let id: String
    let name: String
    let shortName: String
    let startDuration: Int
    let isLoop: Bool
    let attack: Int
    let decay: Int
    let sustain: Double
    let release: Int

    var startUrl: URL? {
        let sampleStartUrl = Bundle.main.url(forResource: id + "_start", withExtension: "wav")
        let sampleLoopUrl = Bundle.main.url(forResource: id + "_loop", withExtension: "wav")

        if let sampleStartPath = sampleStartUrl?.path,
           FileManager.default.fileExists(atPath: sampleStartPath) {
            return sampleStartUrl
        }

        return sampleLoopUrl
    }

    var loopUrl: URL? {
        Bundle.main.url(forResource: id + "_loop", withExtension: "wav")
    }

    init(
        id: String,
        name: String,
        shortName: String,
        startDuration: Int,
        isLoop: Bool,
        attack: Int,
        decay: Int,
        sustain: Double,
        release: Int
    ) {
        self.id = id
        self.name = name
        self.shortName = shortName
        self.startDuration = startDuration
        self.isLoop = isLoop
        self.attack = attack
        self.decay = decay
        self.sustain = sustain
        self.release = release
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    static func == (lhs: Sample, rhs: Sample) -> Bool {
        lhs.id == rhs.id
    }
}
