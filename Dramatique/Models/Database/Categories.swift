import SQLite

final class Categories {
    static let shared = Categories()

    let id = Expression<String>("id")
    let ordinalId = Expression<Int>("ordinalId")
    let name = Expression<String>("name")

    lazy var table = Table("Categories")

    var categories: [Category] {
        do {
            let query = table.order(ordinalId)
            let results = try DatabaseHelper.shared.connection.prepare(query)

            return results.compactMap { Category(id: $0[id], name: $0[name]) }
        } catch {
            print("Error while getting categories:", error)

            return []
        }
    }
}
