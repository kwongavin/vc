import SQLite

final class Samples {
    static let shared = Samples()

    let id = Expression<String>("id")
    let ordinalId = Expression<Int>("ordinalId")
    let name = Expression<String>("name")
    let shortName = Expression<String>("shortName")
    let categoryId = Expression<String>("categoryId")
    let startDuration = Expression<Int>("startDuration")
    let isLoop = Expression<Bool>("isLoop")
    let attack = Expression<Int>("attack")
    let decay = Expression<Int>("decay")
    let sustain = Expression<Double>("sustain")
    let release = Expression<Int>("release")

    lazy var table = Table("Samples")

    func sample(id: String) -> Sample? {
        do {
            let query = table.filter(self.id == id)

            guard let result = try DatabaseHelper.shared.connection.pluck(query) else {
                return nil
            }

            return sample(row: result)
        } catch {
            print("Error while getting sample by ID \(id):", error)

            return nil
        }
    }

    func samples(category: Category) -> [Sample] {
        do {
            let query = table.filter(self.categoryId == category.id).order(ordinalId)
            let results = try DatabaseHelper.shared.connection.prepare(query)

            return results.compactMap { sample(row: $0) }
        } catch {
            print("Error while getting categories:", error)

            return []
        }
    }

    func sample(row: Row) -> Sample {
        .init(
            id: row[id],
            name: row[name],
            shortName: row[shortName],
            startDuration: row[startDuration],
            isLoop: row[isLoop],
            attack: row[attack],
            decay: row[decay],
            sustain: row[sustain],
            release: row[release]
        )
    }
}
