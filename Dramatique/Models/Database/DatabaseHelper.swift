import SQLite

final class DatabaseHelper {
    static let shared = DatabaseHelper()

    var connection: Connection!

    func prepare() {
        do {
            guard let dbPath = Bundle.main.url(forResource: "database", withExtension: "sqlite") else {
                return
            }

            connection = try Connection(dbPath.relativePath)
        } catch {
            print("Error getting connection to SQLite:", error)
        }
    }
}
