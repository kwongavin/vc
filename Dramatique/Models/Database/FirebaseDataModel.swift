//
//  FirebaseDataModel.swift
//  Dramatique
//
//  Created by Deepak Verma on 19/04/22.
//

struct CategoryData {
    var categoryID: String
    var categoryData: [RemoteVideoData]
}

struct RemoteVideoData {
    var categoryID: String
    var videoId: String
    var videoUrl: String
    var externalLink: String
    var messageData: [MessageData]
    var videoLike: [RemoteVideoLikes]
}

struct RemoteVideoLikes {
   var likeId: String
   var likes: Int
}

struct MessageData {
    var messageId: String = ""
    var message: String = ""
}

enum TypeOFVideoData: String {
    case videoComments
    case videoLikes
    case videoUrl
    case description
    case externalLink
}

struct ExternalLink {
    var links: [String]
}
