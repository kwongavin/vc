// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {

  internal enum Categories {
    /// Choose a sample!
    internal static let title = L10n.tr("Localizable", "Categories.title")
  }

  internal enum CommentView {
    /// Alert
    internal static let alert = L10n.tr("Localizable", "CommentView.alert")
    /// Please enter message!
    internal static let message = L10n.tr("Localizable", "CommentView.message")
    /// ok
    internal static let ok = L10n.tr("Localizable", "CommentView.ok")
    /// Send message...
    internal static let placeholder = L10n.tr("Localizable", "CommentView.placeholder")
  }

  internal enum Drawer {
    internal enum Bottom {
      /// Cancel
      internal static let cancel = L10n.tr("Localizable", "Drawer.Bottom.cancel")
      /// Link
      internal static let link = L10n.tr("Localizable", "Drawer.Bottom.Link")
      /// Next
      internal static let next = L10n.tr("Localizable", "Drawer.Bottom.next")
      /// PD Mode
      internal static let pdMode = L10n.tr("Localizable", "Drawer.Bottom.PDMode")
      /// Render Audio Only
      internal static let renderAudioOnly = L10n.tr("Localizable", "Drawer.Bottom.renderAudioOnly")
      /// Render Video
      internal static let renderVideo = L10n.tr("Localizable", "Drawer.Bottom.renderVideo")
      /// Reset
      internal static let reset = L10n.tr("Localizable", "Drawer.Bottom.reset")
      /// Solo Off
      internal static let soloOff = L10n.tr("Localizable", "Drawer.Bottom.soloOff")
      /// Yes, Solo Off!
      internal static let soloOffChange = L10n.tr("Localizable", "Drawer.Bottom.soloOffChange")
      /// Solo On
      internal static let soloOn = L10n.tr("Localizable", "Drawer.Bottom.soloOn")
      /// Yes, Solo On!
      internal static let soloOnChange = L10n.tr("Localizable", "Drawer.Bottom.soloOnChange")
      /// THR Mode
      internal static let thrMode = L10n.tr("Localizable", "Drawer.Bottom.THRMode")
      internal enum ResetAlert {
        /// No
        internal static let no = L10n.tr("Localizable", "Drawer.Bottom.ResetAlert.no")
        /// Are you sure to reset?
        internal static let title = L10n.tr("Localizable", "Drawer.Bottom.ResetAlert.title")
        /// Yes
        internal static let yes = L10n.tr("Localizable", "Drawer.Bottom.ResetAlert.yes")
      }
    }
  }

  internal enum MessageBoard {
    /// Audio saved to your library!
    internal static let audioSaved = L10n.tr("Localizable", "MessageBoard.audioSaved")
    /// Now, tap + to choose a sample!
    internal static let chooseSample = L10n.tr("Localizable", "MessageBoard.chooseSample")
    /// Actually, multiple samples.
    internal static let chooseSample2 = L10n.tr("Localizable", "MessageBoard.chooseSample2")
    /// Exporting... %@
    internal static func exporting(_ p1: Any) -> String {
      return L10n.tr("Localizable", "MessageBoard.exporting", String(describing: p1))
    }
    /// Tap to select a video from library!
    internal static let loadVideo = L10n.tr("Localizable", "MessageBoard.loadVideo")
    /// Original volume: %@
    internal static func originalVolume(_ p1: Any) -> String {
      return L10n.tr("Localizable", "MessageBoard.originalVolume", String(describing: p1))
    }
    /// Playing video ...
    internal static let playing = L10n.tr("Localizable", "MessageBoard.playing")
    /// Playing stopped
    internal static let playingStopped = L10n.tr("Localizable", "MessageBoard.playingStopped")
    /// Tap the REC Button when you’re ready!
    internal static let pressRecord = L10n.tr("Localizable", "MessageBoard.pressRecord")
    /// Recording samples ...
    internal static let recording = L10n.tr("Localizable", "MessageBoard.recording")
    /// Recording stopped
    internal static let recordingStopped = L10n.tr("Localizable", "MessageBoard.recordingStopped")
    /// Render Settings
    internal static let renderSettings = L10n.tr("Localizable", "MessageBoard.renderSettings")
    /// %@ loaded
    internal static func sampleLoaded(_ p1: Any) -> String {
      return L10n.tr("Localizable", "MessageBoard.sampleLoaded", String(describing: p1))
    }
    /// Sample volume: %@
    internal static func sampleVolume(_ p1: Any) -> String {
      return L10n.tr("Localizable", "MessageBoard.sampleVolume", String(describing: p1))
    }
    /// Solo Mode Off
    internal static let soloModeOff = L10n.tr("Localizable", "MessageBoard.soloModeOff")
    /// Solo Mode On
    internal static let soloModeOn = L10n.tr("Localizable", "MessageBoard.soloModeOn")
    /// Try as many takes as you like!
    internal static let takeMoreTakes = L10n.tr("Localizable", "MessageBoard.takeMoreTakes")
    /// Video saved to your library!
    internal static let videoSaved = L10n.tr("Localizable", "MessageBoard.videoSaved")
    /// Messsage1
    internal static let welcome = L10n.tr("Localizable", "MessageBoard.welcome")
    /// Messsage2
    internal static let welcome2 = L10n.tr("Localizable", "MessageBoard.welcome2")
    /// Messsage3
    internal static let welcome3 = L10n.tr("Localizable", "MessageBoard.welcome3")
    /// Messsage4
    internal static let welcome4 = L10n.tr("Localizable", "MessageBoard.welcome4")
    internal enum UndoRedo {
      /// %@ recording sample
      internal static func recording(_ p1: Any) -> String {
        return L10n.tr("Localizable", "MessageBoard.UndoRedo.recording", String(describing: p1))
      }
      /// Redo
      internal static let redo = L10n.tr("Localizable", "MessageBoard.UndoRedo.redo")
      /// %@ loading %@
      internal static func sample(_ p1: Any, _ p2: Any) -> String {
        return L10n.tr("Localizable", "MessageBoard.UndoRedo.sample", String(describing: p1), String(describing: p2))
      }
      /// Undo
      internal static let undo = L10n.tr("Localizable", "MessageBoard.UndoRedo.undo")
    }
  }

  internal enum MessageBoardTheatre {
    /// Welcome to theatre mode
    internal static let welcome = L10n.tr("Localizable", "MessageBoardTheatre.welcome")
  }

  internal enum Record {
    internal enum OverrideAlert {
      /// No
      internal static let no = L10n.tr("Localizable", "Record.OverrideAlert.no")
      /// New recording will replace the old ones. Are you sure to continue?
      internal static let title = L10n.tr("Localizable", "Record.OverrideAlert.title")
      /// Yes
      internal static let yes = L10n.tr("Localizable", "Record.OverrideAlert.yes")
    }
  }

  internal enum RenderSettings {
    /// Render Audio Only
    internal static let renderAudioOnly = L10n.tr("Localizable", "RenderSettings.renderAudioOnly")
    /// Render Video
    internal static let renderVideo = L10n.tr("Localizable", "RenderSettings.renderVideo")
  }

  internal enum Sample {
    internal enum OverrideAlert {
      /// No
      internal static let no = L10n.tr("Localizable", "Sample.OverrideAlert.no")
      /// Replacing a sample will clear the track. Are you sure to continue?
      internal static let title = L10n.tr("Localizable", "Sample.OverrideAlert.title")
      /// Yes
      internal static let yes = L10n.tr("Localizable", "Sample.OverrideAlert.yes")
    }
  }

  internal enum Settings {
    /// Solo Mode
    internal static let soloMode = L10n.tr("Localizable", "Settings.soloMode")
    /// Settings
    internal static let title = L10n.tr("Localizable", "Settings.title")
  }

  internal enum TopView {
    /// Please check your internet connection
    internal static let alertMessage = L10n.tr("Localizable", "TopView.alertMessage")
    /// Ok
    internal static let alertOk = L10n.tr("Localizable", "TopView.alertOk")
    /// No Internet
    internal static let alertTitle = L10n.tr("Localizable", "TopView.alertTitle")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
