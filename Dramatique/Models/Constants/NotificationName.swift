import Foundation

final class NotificationNameObject {
    let id: String

    var name: Notification.Name {
        Notification.Name(id)
    }

    init(_ id: String) {
        self.id = id
    }
}

struct NotificationName {
    static let changeOriginalVolume = NotificationNameObject("changeOriginalVolume")
    static let changeSampleVolume = NotificationNameObject("changeSampleVolume")
    static let changeVideoCurrentTime = NotificationNameObject("changeVideoCurrentTime")
    static let ignoreOriginalVolumeRequests = NotificationNameObject("ignoreOriginalVolumeRequests")
    static let loadVideo = NotificationNameObject("loadVideo")
    static let pauseVideo = NotificationNameObject("pauseVideo")
    static let playVideo = NotificationNameObject("playVideo")
    static let reset = NotificationNameObject("reset")
    static let seekToStartOfVideo = NotificationNameObject("seekToStartOfVideo")
    static let stopIgnoringOriginalVolumeRequests = NotificationNameObject("stopIgnoringOriginalVolumeRequests")
    static let toggleVideoPlayback = NotificationNameObject("toggleVideoPlayback")
}

struct UserInfoConstants {
    static let adsrType = "adsrType"
    static let currentTime = "currentTime"
    static let isLoopingAllowed = "isLoopingAllowed"
    static let player = "player"
    static let sample = "sample"
    static let sampleIndex = "sampleIndex"
    static let volume = "volume"
}
