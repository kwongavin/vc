// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let accentColor = ColorAsset(name: "AccentColor")
  internal static let appYellow = ColorAsset(name: "appYellow")
  internal static let backgroundDarkBlue = ColorAsset(name: "backgroundDarkBlue")
  internal static let backgroundDarkBlueShadeFour = ColorAsset(name: "backgroundDarkBlueShadeFour")
  internal static let backgroundDarkBlueShadeThree = ColorAsset(name: "backgroundDarkBlueShadeThree")
  internal static let backgroundDarkBlueShadeTwo = ColorAsset(name: "backgroundDarkBlueShadeTwo")
  internal static let backgroundGreen = ColorAsset(name: "backgroundGreen")
  internal static let backgroundGreenShapeThree = ColorAsset(name: "backgroundGreenShapeThree")
  internal static let backgroundGreenShapeTwo = ColorAsset(name: "backgroundGreenShapeTwo")
  internal static let lightBackgroundDarkBlue = ColorAsset(name: "lightBackgroundDarkBlue")
  internal static let lightBackgroundDarkBlueShapeThree = ColorAsset(name: "lightBackgroundDarkBlueShapeThree")
  internal static let lightBackgroundDarkBlueShapeTwo = ColorAsset(name: "lightBackgroundDarkBlueShapeTwo")
  internal static let uiTableViewBackgroundDarkBlue = ColorAsset(name: "uiTableViewBackgroundDarkBlue")
  internal static let pdModeBack = ImageAsset(name: "PD_Mode_Back")
  internal static let thrComment = ImageAsset(name: "THR_Comment")
  internal static let thrModeBack = ImageAsset(name: "THR_Mode_Back")
  internal static let artside1 = ImageAsset(name: "artside1")
  internal static let artside10 = ImageAsset(name: "artside10")
  internal static let artside11 = ImageAsset(name: "artside11")
  internal static let artside12 = ImageAsset(name: "artside12")
  internal static let artside13 = ImageAsset(name: "artside13")
  internal static let artside2 = ImageAsset(name: "artside2")
  internal static let artside3 = ImageAsset(name: "artside3")
  internal static let artside4 = ImageAsset(name: "artside4")
  internal static let artside5 = ImageAsset(name: "artside5")
  internal static let artside6 = ImageAsset(name: "artside6")
  internal static let artside7 = ImageAsset(name: "artside7")
  internal static let artside8 = ImageAsset(name: "artside8")
  internal static let artside9 = ImageAsset(name: "artside9")
  internal static let classy1 = ImageAsset(name: "classy1")
  internal static let classy10 = ImageAsset(name: "classy10")
  internal static let classy11 = ImageAsset(name: "classy11")
  internal static let classy12 = ImageAsset(name: "classy12")
  internal static let classy13 = ImageAsset(name: "classy13")
  internal static let classy14 = ImageAsset(name: "classy14")
  internal static let classy15 = ImageAsset(name: "classy15")
  internal static let classy16 = ImageAsset(name: "classy16")
  internal static let classy17 = ImageAsset(name: "classy17")
  internal static let classy18 = ImageAsset(name: "classy18")
  internal static let classy2 = ImageAsset(name: "classy2")
  internal static let classy3 = ImageAsset(name: "classy3")
  internal static let classy4 = ImageAsset(name: "classy4")
  internal static let classy5 = ImageAsset(name: "classy5")
  internal static let classy6 = ImageAsset(name: "classy6")
  internal static let classy7 = ImageAsset(name: "classy7")
  internal static let classy8 = ImageAsset(name: "classy8")
  internal static let classy9 = ImageAsset(name: "classy9")
  internal static let close = ImageAsset(name: "close")
  internal static let delete = ImageAsset(name: "delete")
  internal static let fullPreview = ImageAsset(name: "fullPreview")
  internal static let hollywood1 = ImageAsset(name: "hollywood1")
  internal static let hollywood10 = ImageAsset(name: "hollywood10")
  internal static let hollywood11 = ImageAsset(name: "hollywood11")
  internal static let hollywood12 = ImageAsset(name: "hollywood12")
  internal static let hollywood13 = ImageAsset(name: "hollywood13")
  internal static let hollywood14 = ImageAsset(name: "hollywood14")
  internal static let hollywood15 = ImageAsset(name: "hollywood15")
  internal static let hollywood16 = ImageAsset(name: "hollywood16")
  internal static let hollywood17 = ImageAsset(name: "hollywood17")
  internal static let hollywood18 = ImageAsset(name: "hollywood18")
  internal static let hollywood19 = ImageAsset(name: "hollywood19")
  internal static let hollywood2 = ImageAsset(name: "hollywood2")
  internal static let hollywood20 = ImageAsset(name: "hollywood20")
  internal static let hollywood21 = ImageAsset(name: "hollywood21")
  internal static let hollywood3 = ImageAsset(name: "hollywood3")
  internal static let hollywood4 = ImageAsset(name: "hollywood4")
  internal static let hollywood5 = ImageAsset(name: "hollywood5")
  internal static let hollywood6 = ImageAsset(name: "hollywood6")
  internal static let hollywood7 = ImageAsset(name: "hollywood7")
  internal static let hollywood8 = ImageAsset(name: "hollywood8")
  internal static let hollywood9 = ImageAsset(name: "hollywood9")
  internal static let hongKong1 = ImageAsset(name: "hong_kong1")
  internal static let hongKong10 = ImageAsset(name: "hong_kong10")
  internal static let hongKong11 = ImageAsset(name: "hong_kong11")
  internal static let hongKong2 = ImageAsset(name: "hong_kong2")
  internal static let hongKong3 = ImageAsset(name: "hong_kong3")
  internal static let hongKong4 = ImageAsset(name: "hong_kong4")
  internal static let hongKong5 = ImageAsset(name: "hong_kong5")
  internal static let hongKong6 = ImageAsset(name: "hong_kong6")
  internal static let hongKong7 = ImageAsset(name: "hong_kong7")
  internal static let hongKong8 = ImageAsset(name: "hong_kong8")
  internal static let hongKong9 = ImageAsset(name: "hong_kong9")
  internal static let kDrama1 = ImageAsset(name: "k_drama1")
  internal static let kDrama10 = ImageAsset(name: "k_drama10")
  internal static let kDrama11 = ImageAsset(name: "k_drama11")
  internal static let kDrama12 = ImageAsset(name: "k_drama12")
  internal static let kDrama13 = ImageAsset(name: "k_drama13")
  internal static let kDrama14 = ImageAsset(name: "k_drama14")
  internal static let kDrama15 = ImageAsset(name: "k_drama15")
  internal static let kDrama16 = ImageAsset(name: "k_drama16")
  internal static let kDrama17 = ImageAsset(name: "k_drama17")
  internal static let kDrama18 = ImageAsset(name: "k_drama18")
  internal static let kDrama19 = ImageAsset(name: "k_drama19")
  internal static let kDrama2 = ImageAsset(name: "k_drama2")
  internal static let kDrama20 = ImageAsset(name: "k_drama20")
  internal static let kDrama21 = ImageAsset(name: "k_drama21")
  internal static let kDrama22 = ImageAsset(name: "k_drama22")
  internal static let kDrama23 = ImageAsset(name: "k_drama23")
  internal static let kDrama24 = ImageAsset(name: "k_drama24")
  internal static let kDrama25 = ImageAsset(name: "k_drama25")
  internal static let kDrama26 = ImageAsset(name: "k_drama26")
  internal static let kDrama3 = ImageAsset(name: "k_drama3")
  internal static let kDrama4 = ImageAsset(name: "k_drama4")
  internal static let kDrama5 = ImageAsset(name: "k_drama5")
  internal static let kDrama6 = ImageAsset(name: "k_drama6")
  internal static let kDrama7 = ImageAsset(name: "k_drama7")
  internal static let kDrama8 = ImageAsset(name: "k_drama8")
  internal static let kDrama9 = ImageAsset(name: "k_drama9")
  internal static let library = ImageAsset(name: "library")
  internal static let light = ImageAsset(name: "light")
  internal static let link = ImageAsset(name: "link")
  internal static let loFi1 = ImageAsset(name: "lo_fi1")
  internal static let loFi10 = ImageAsset(name: "lo_fi10")
  internal static let loFi11 = ImageAsset(name: "lo_fi11")
  internal static let loFi12 = ImageAsset(name: "lo_fi12")
  internal static let loFi13 = ImageAsset(name: "lo_fi13")
  internal static let loFi14 = ImageAsset(name: "lo_fi14")
  internal static let loFi15 = ImageAsset(name: "lo_fi15")
  internal static let loFi16 = ImageAsset(name: "lo_fi16")
  internal static let loFi17 = ImageAsset(name: "lo_fi17")
  internal static let loFi2 = ImageAsset(name: "lo_fi2")
  internal static let loFi3 = ImageAsset(name: "lo_fi3")
  internal static let loFi4 = ImageAsset(name: "lo_fi4")
  internal static let loFi5 = ImageAsset(name: "lo_fi5")
  internal static let loFi6 = ImageAsset(name: "lo_fi6")
  internal static let loFi7 = ImageAsset(name: "lo_fi7")
  internal static let loFi8 = ImageAsset(name: "lo_fi8")
  internal static let loFi9 = ImageAsset(name: "lo_fi9")
  internal static let memeable1 = ImageAsset(name: "memeable1")
  internal static let memeable10 = ImageAsset(name: "memeable10")
  internal static let memeable11 = ImageAsset(name: "memeable11")
  internal static let memeable12 = ImageAsset(name: "memeable12")
  internal static let memeable13 = ImageAsset(name: "memeable13")
  internal static let memeable14 = ImageAsset(name: "memeable14")
  internal static let memeable15 = ImageAsset(name: "memeable15")
  internal static let memeable16 = ImageAsset(name: "memeable16")
  internal static let memeable17 = ImageAsset(name: "memeable17")
  internal static let memeable18 = ImageAsset(name: "memeable18")
  internal static let memeable19 = ImageAsset(name: "memeable19")
  internal static let memeable2 = ImageAsset(name: "memeable2")
  internal static let memeable20 = ImageAsset(name: "memeable20")
  internal static let memeable21 = ImageAsset(name: "memeable21")
  internal static let memeable22 = ImageAsset(name: "memeable22")
  internal static let memeable23 = ImageAsset(name: "memeable23")
  internal static let memeable24 = ImageAsset(name: "memeable24")
  internal static let memeable25 = ImageAsset(name: "memeable25")
  internal static let memeable26 = ImageAsset(name: "memeable26")
  internal static let memeable27 = ImageAsset(name: "memeable27")
  internal static let memeable28 = ImageAsset(name: "memeable28")
  internal static let memeable3 = ImageAsset(name: "memeable3")
  internal static let memeable4 = ImageAsset(name: "memeable4")
  internal static let memeable5 = ImageAsset(name: "memeable5")
  internal static let memeable6 = ImageAsset(name: "memeable6")
  internal static let memeable7 = ImageAsset(name: "memeable7")
  internal static let memeable8 = ImageAsset(name: "memeable8")
  internal static let memeable9 = ImageAsset(name: "memeable9")
  internal static let netflix1 = ImageAsset(name: "netflix1")
  internal static let netflix10 = ImageAsset(name: "netflix10")
  internal static let netflix11 = ImageAsset(name: "netflix11")
  internal static let netflix12 = ImageAsset(name: "netflix12")
  internal static let netflix13 = ImageAsset(name: "netflix13")
  internal static let netflix14 = ImageAsset(name: "netflix14")
  internal static let netflix15 = ImageAsset(name: "netflix15")
  internal static let netflix16 = ImageAsset(name: "netflix16")
  internal static let netflix17 = ImageAsset(name: "netflix17")
  internal static let netflix18 = ImageAsset(name: "netflix18")
  internal static let netflix19 = ImageAsset(name: "netflix19")
  internal static let netflix2 = ImageAsset(name: "netflix2")
  internal static let netflix20 = ImageAsset(name: "netflix20")
  internal static let netflix3 = ImageAsset(name: "netflix3")
  internal static let netflix4 = ImageAsset(name: "netflix4")
  internal static let netflix5 = ImageAsset(name: "netflix5")
  internal static let netflix6 = ImageAsset(name: "netflix6")
  internal static let netflix7 = ImageAsset(name: "netflix7")
  internal static let netflix8 = ImageAsset(name: "netflix8")
  internal static let netflix9 = ImageAsset(name: "netflix9")
  internal static let originalVolume = ImageAsset(name: "original_volume")
  internal static let pause = ImageAsset(name: "pause")
  internal static let play = ImageAsset(name: "play")
  internal static let record = ImageAsset(name: "record")
  internal static let redo = ImageAsset(name: "redo")
  internal static let reset = ImageAsset(name: "reset")
  internal static let sampleAdd = ImageAsset(name: "sample_add")
  internal static let sampleVolume = ImageAsset(name: "sample_volume")
  internal static let stop = ImageAsset(name: "stop")
  internal static let subtle1 = ImageAsset(name: "subtle1")
  internal static let subtle10 = ImageAsset(name: "subtle10")
  internal static let subtle11 = ImageAsset(name: "subtle11")
  internal static let subtle12 = ImageAsset(name: "subtle12")
  internal static let subtle13 = ImageAsset(name: "subtle13")
  internal static let subtle14 = ImageAsset(name: "subtle14")
  internal static let subtle15 = ImageAsset(name: "subtle15")
  internal static let subtle16 = ImageAsset(name: "subtle16")
  internal static let subtle17 = ImageAsset(name: "subtle17")
  internal static let subtle18 = ImageAsset(name: "subtle18")
  internal static let subtle19 = ImageAsset(name: "subtle19")
  internal static let subtle2 = ImageAsset(name: "subtle2")
  internal static let subtle20 = ImageAsset(name: "subtle20")
  internal static let subtle21 = ImageAsset(name: "subtle21")
  internal static let subtle22 = ImageAsset(name: "subtle22")
  internal static let subtle23 = ImageAsset(name: "subtle23")
  internal static let subtle24 = ImageAsset(name: "subtle24")
  internal static let subtle25 = ImageAsset(name: "subtle25")
  internal static let subtle26 = ImageAsset(name: "subtle26")
  internal static let subtle27 = ImageAsset(name: "subtle27")
  internal static let subtle28 = ImageAsset(name: "subtle28")
  internal static let subtle29 = ImageAsset(name: "subtle29")
  internal static let subtle3 = ImageAsset(name: "subtle3")
  internal static let subtle4 = ImageAsset(name: "subtle4")
  internal static let subtle5 = ImageAsset(name: "subtle5")
  internal static let subtle6 = ImageAsset(name: "subtle6")
  internal static let subtle7 = ImageAsset(name: "subtle7")
  internal static let subtle8 = ImageAsset(name: "subtle8")
  internal static let subtle9 = ImageAsset(name: "subtle9")
  internal static let undo = ImageAsset(name: "undo")
  internal static let videoPlaceholder = ImageAsset(name: "video_placeholder")
  internal static let watermark = ImageAsset(name: "watermark")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = {
    guard let color = Color(asset: self) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }()

  #if os(iOS) || os(tvOS)
  @available(iOS 11.0, tvOS 11.0, *)
  internal func color(compatibleWith traitCollection: UITraitCollection) -> Color {
    let bundle = BundleToken.bundle
    guard let color = Color(named: name, in: bundle, compatibleWith: traitCollection) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }
  #endif

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init?(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  @available(iOS 8.0, tvOS 9.0, watchOS 2.0, macOS 10.7, *)
  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }

  #if os(iOS) || os(tvOS)
  @available(iOS 8.0, tvOS 9.0, *)
  internal func image(compatibleWith traitCollection: UITraitCollection) -> Image {
    let bundle = BundleToken.bundle
    guard let result = Image(named: name, in: bundle, compatibleWith: traitCollection) else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
  #endif
}

internal extension ImageAsset.Image {
  @available(iOS 8.0, tvOS 9.0, watchOS 2.0, *)
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
