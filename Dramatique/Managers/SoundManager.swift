import AVFoundation

final class SoundManager {
    private let appManager = AppManager.shared

    static let shared = SoundManager()

    // index -1 is for previews
    var startPlayersForIndices = [Int: CustomPlayer]()
    var loopPlayersForIndices = [Int: CustomQueuePlayer]()

    var playerLoopers = [CustomQueuePlayer: AVPlayerLooper]()

    var loopTimers = [CustomQueuePlayer: Timer]()
    var releaseTimers = [CustomPlayer: Timer]()

    let bitrate: Double = 44100

    struct LoopTimerUserInfo {
        let loopPlayer: CustomQueuePlayer
        let sampleIndex: Int
    }

    struct ReleaseTimerUserInfo {
        let startPlayer: CustomPlayer
        let sampleIndex: Int
    }

    func playSound(sampleIndex: Int = -1) {
        let sampleOrNil: Sample? = {
            if sampleIndex != -1 {
                return SampleManager.shared.selectedSamples[sampleIndex]
            }

            return SampleManager.shared.sampleToPreview
        }()

        guard let sample = sampleOrNil, let startUrl = sample.startUrl else { return }

        let startPlayer = CustomPlayer(url: startUrl)

        let volume = sampleIndex == -1 ? 1 : Float(appManager.sampleVolume)

        startPlayer.volume = 0

        startPlayer.play()

        startPlayer.setVolume(volume, duration: Float(sample.attack) / Float(bitrate)) {
            startPlayer.setVolume(Float(sample.sustain) * volume, duration: Float(sample.decay) / Float(self.bitrate))
        }

        startPlayersForIndices[sampleIndex] = startPlayer

        if sample.isLoop {
            guard let loopUrl = sample.loopUrl else {
                return
            }

            let loopPlayer = CustomQueuePlayer(url: loopUrl)
            loopPlayer.volume = Float(sample.sustain) * volume
            loopPlayersForIndices[sampleIndex] = loopPlayer

            if let currentItem = loopPlayer.currentItem {
                let playerLooper = AVPlayerLooper(player: loopPlayer, templateItem: currentItem)

                playerLoopers[loopPlayer] = playerLooper
            }

            let userInfo = LoopTimerUserInfo(loopPlayer: loopPlayer, sampleIndex: sampleIndex)

            let loopTimer = Timer.scheduledTimer(timeInterval: .init(sample.startDuration) / .init(bitrate), target: self, selector: #selector(handleLoopTimer(_:)), userInfo: userInfo, repeats: false)

            loopTimers[loopPlayer] = loopTimer
        } else {
            let userInfo = ReleaseTimerUserInfo(startPlayer: startPlayer, sampleIndex: sampleIndex)

            let releaseTimer = Timer.scheduledTimer(timeInterval: .init(sample.startDuration - sample.release) / .init(bitrate), target: self, selector: #selector(handleReleaseTimer(_:)), userInfo: userInfo, repeats: false)

            releaseTimers[startPlayer] = releaseTimer
        }
    }

    func changeVolume(volume: Float) {
        for player in startPlayersForIndices.values {
            player.volume = volume
        }

        for (key, value) in loopPlayersForIndices {
            guard key != -1, let sample = SampleManager.shared.selectedSamples[key] else { continue }

            value.volume = volume * Float(sample.sustain)
        }
    }

    func stopSound(sampleIndex: Int = -1) {
        let sampleOrNil: Sample? = {
            guard sampleIndex != -1 else { return SampleManager.shared.sampleToPreview }

            return SampleManager.shared.selectedSamples[sampleIndex]
        }()

        guard let sample = sampleOrNil else { return }

        if let player = startPlayersForIndices[sampleIndex] {
            player.setVolume(0, duration: Float(sample.release) / Float(bitrate)) {
                player.pause()

                self.releaseTimers[player]?.invalidate()
                self.releaseTimers.removeValue(forKey: player)
            }
        }

        if let player = loopPlayersForIndices[sampleIndex] {
            player.setVolume(0, duration: Float(sample.release) / Float(bitrate)) {
                player.pause()

                self.loopTimers[player]?.invalidate()
                self.loopTimers.removeValue(forKey: player)

                self.playerLoopers.removeValue(forKey: player)
            }
        }
    }

    func stopAllSounds() {
        for sampleIndex in 0 ..< SampleManager.sampleCount {
            stopSound(sampleIndex: sampleIndex)
        }
    }

    @objc private func handleLoopTimer(_ timer: Timer) {
        guard let userInfo = timer.userInfo as? LoopTimerUserInfo else {
            return
        }

        userInfo.loopPlayer.play()

        loopTimers.removeValue(forKey: userInfo.loopPlayer)
    }

    @objc private func handleReleaseTimer(_ timer: Timer) {
        guard let userInfo = timer.userInfo as? ReleaseTimerUserInfo else {
            return
        }

        let sampleOrNil: Sample? = {
            guard userInfo.sampleIndex != -1 else { return SampleManager.shared.sampleToPreview }

            return SampleManager.shared.selectedSamples[userInfo.sampleIndex]
        }()

        guard let sample = sampleOrNil else { return }

        userInfo.startPlayer.setVolume(0, duration: Float(sample.release / Int(bitrate))) { [weak self] in
            userInfo.startPlayer.pause()

            self?.startPlayersForIndices.removeValue(forKey: userInfo.sampleIndex)
        }

        releaseTimers.removeValue(forKey: userInfo.startPlayer)
    }

    func reset() {
        startPlayersForIndices.values.forEach { $0.pause() }
        startPlayersForIndices.removeAll()

        loopPlayersForIndices.values.forEach { $0.pause() }
        loopPlayersForIndices.removeAll()
    }
}
