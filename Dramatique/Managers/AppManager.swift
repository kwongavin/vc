import SwiftUI

final class AppManager: ObservableObject {
    static let shared = AppManager()

    @Published var isPreparing = true
    @Published var isSoloModeOn = true

    @Published var showSoloModeOnMessage = false
    @Published var isChangingOriginalVolume = false
    @Published var isChangingSampleVolume = false
    @Published var shouldShowResetAlert = false
    @Published var shouldShowRenderSettings = false
    @Published var shouldAnimateDrawer = false
    @Published var showTheatreMode = false
    @Published var commentButtonPressed = false
    @Published var isExternalLink = false
    @Published var isLikeExist = false
    @Published var showInternetAlert = false
    @Published var isFullPreviewEnabled = false
    @Published var updateSubCategories = false

    @Published var size = CGSize()
    @Published var safeAreaInsets = EdgeInsets()

    @Published var videoCategory : SelectedVideoCategory = .all
    @Published var videoCategorySelected : SelectedVideoCategory = .all

    @Published var categorySelected1 = false
    @Published var categorySelected2 = false
    @Published var categorySelected3 = false

    @Published var originalVolume: Double = 1
    @Published var sampleVolume: Double = 1

    @Published var videoCounter = 0
    @Published var messageCounter = 0
    @Published var externalLinkCounter = 0
    @Published var videoCount = 0 // this counter is used as video counter helper if video count is ended

    @Published var allVideoData = [CategoryData]()
    @Published var externalLinks: CategoryData?
    @Published var isMessageIsMine = false

    let networkConnectionManager = NetworkConnectionManager()
    let firebaseManager = FirebaseManager()

    private var isRepeat = false
    private var isFirstTime = false
    private var isFirstTimeExternalLink = false

    var isValidVideoCounter : Bool {
        if allVideoData.count > 0 {
            return allVideoData[videoCategory.rawValue].categoryData.count > videoCounter
        } else {
            return false
        }
    }

    var category : String {
        (videoCategory == .all || videoCategory == .category1_2 || videoCategory ==  .category2_3 || videoCategory == .category3_1 ) ? "\(allVideoData[videoCategory.rawValue].categoryData[videoCounter].categoryID)" : "\(allVideoData[videoCategory.rawValue].categoryID)"
    }

    var isNoMessages: Bool {
        allVideoData[videoCategory.rawValue].categoryData[videoCounter].messageData.isEmpty
    }

    private var videoID : String {
        "\(allVideoData[videoCategory.rawValue].categoryData[videoCounter].videoId)"
    }

    private var messageID: String {
        if isNoMessages  {
            return "isNoMessages"
        }else {
            if allVideoData[videoCategory.rawValue].categoryData[videoCounter].messageData.count > messageCounter {
                return "\(allVideoData[videoCategory.rawValue].categoryData[videoCounter].messageData[messageCounter].messageId)"
            }else{
                return "isNoMessages"
            }
        }
    }

    // MARK: - Intializers

    init() {
        getFirebaseData()
    }

    // MARK: - Functions

    func reset() {
        originalVolume = showTheatreMode ? originalVolume : 1
        sampleVolume = 1
        shouldShowRenderSettings = false

        CompositionManager.shared.reset()
        RecordingManager.shared.reset()
        SampleManager.shared.reset()
        UndoManager.shared.reset()
        VideoManager.shared.reset()

        NotificationCenter.default.post(name: NotificationName.reset.name, object: nil)
    }

    func widthWithMargin(_ margin: CGFloat) -> CGFloat {
        max(0, size.width - safeAreaInsets.leading - safeAreaInsets.trailing - margin * 2)
    }

    func pullToRefresh() {
        reset()

        switch networkConnectionManager.connection {
        case .unavailable:
            showInternetAlert = true
        default:
            checkAndSetCategories()

            if isValidVideoCounter {
                updateVideoCounter()
            } else {
                videoCounter = 0
            }

            loadVideoFromFirebase()
        }

        messageCounter = 0
        commentButtonPressed = false
    }

    func checkAndSetCategories() {
        if videoCategory != videoCategorySelected {
            externalLinkCounter = 0
            videoCounter = 0
            videoCategory = videoCategorySelected
            isFirstTime = false
            isFirstTimeExternalLink = false
        }

//        if (!categorySelected1 && !categorySelected2 && !categorySelected3) || (categorySelected1 && categorySelected2 && categorySelected3) {
//            setCategory(type: .all)
//        }
//        else if categorySelected1 && categorySelected2 {
//            setCategory(type: .category1_2)
//            sortArrayBottomToTop(type: .category1_2)
//        } else if categorySelected2 && categorySelected3 {
//            setCategory(type: .category2_3)
//            sortArrayBottomToTop(type: .category2_3)
//        } else if categorySelected3 && categorySelected1 {
//            setCategory(type: .category3_1)
//            sortArrayBottomToTop(type: .category3_1)
//        } else if categorySelected1 {
//            setCategory(type: .category1)
//        } else if categorySelected2 {
//            setCategory(type: .category2)
//        } else if categorySelected3 {
//            setCategory(type: .category3)
//        }
        
            if (!categorySelected1 && !categorySelected2 && !categorySelected3){
                setCategory(type: .all)
            }else 
            if categorySelected1 {
                setCategory(type: .category1)
            } else if categorySelected2 {
                setCategory(type: .category2)
            } else if categorySelected3 {
                setCategory(type: .category3)
            }
    }

    private func setCategory(type: SelectedVideoCategory) {
        videoCategory = type
        videoCategorySelected = type
    }

    private func sortArrayBottomToTop(type : SelectedVideoCategory) {
        allVideoData[type.rawValue].categoryData = allVideoData[type.rawValue].categoryData.sorted(by: { data1, data2 in
            let value = data1.categoryID.replacingOccurrences(of: "category", with: "")
            let value1 = data2.categoryID.replacingOccurrences(of: "category", with: "")
            return value > value1
        })
    }

    func loadVideoFromFirebase() {
        isExternalLink = videoCount % 5 == 0 && videoCounter != 0

        if !isRepeat && videoCount > 6 {
            videoCount = videoCount - 1
            isExternalLink = false
            isRepeat = true
        }

        reset()
        loadVideo()
    }

    private func loadVideo() {
        if isExternalLink {
            updateExternalLinkCounter()
            isRepeat = false
            videoCounter = videoCounter - 1

            if !(externalLinks?.categoryData.isEmpty ?? true) {
                guard  let urlString = externalLinks?.categoryData[externalLinkCounter].videoUrl else { return }

                VideoManager.shared.url = URL(string: urlString)
                VideoManager.shared.load()
            }
        } else {
            if !allVideoData.isEmpty {
                let urlString = allVideoData[videoCategory.rawValue].categoryData[videoCounter].videoUrl

                VideoManager.shared.url = URL(string: urlString)
                VideoManager.shared.load()
            }
        }
    }

    func updateVideoCounter() {
        if isFirstTime {
            if allVideoData[videoCategory.rawValue].categoryData.count == 1 {
                videoCounter = 0
            } else {
                videoCounter = videoCounter + 1

                if !isValidVideoCounter {
                    videoCounter = 0
                }
            }

            videoCount += 1
        }

        isFirstTime = true
    }

    private func updateExternalLinkCounter() {
        if isFirstTimeExternalLink {
            if externalLinks?.categoryData.count == 1 {
                externalLinkCounter = 0
            } else {
                externalLinkCounter = externalLinkCounter + 1
                if externalLinkCounter >= externalLinks?.categoryData.count ?? 0 {
                    externalLinkCounter = 0
                }
            }
        }

        isFirstTimeExternalLink = true
    }

    private func getFirebaseData() {
        firebaseManager.fetchRemoteVideoData { [weak self] allVideoData in
            var allVideoData = allVideoData

            allVideoData = allVideoData.sorted { categoryData, categoryData1 in
                let value = categoryData.categoryID.replacingOccurrences(of: "category", with: "")
                let value1 = categoryData1.categoryID.replacingOccurrences(of: "category", with: "")
                return value < value1
            }
            let index = allVideoData.firstIndex { categoryData in
                categoryData.categoryID == "category4"
            }

            if let index = index {
                self?.externalLinks = CategoryData(categoryID: allVideoData[index].categoryID, categoryData: allVideoData[index].categoryData)
                allVideoData.remove(at: index)
            }

            allVideoData.reverse()
            var foruthIndexVideoData = [RemoteVideoData]()

            for i in 0..<allVideoData.count {
                foruthIndexVideoData.append(contentsOf: allVideoData[i].categoryData)
            }

            let categoryDataAll = CategoryData(categoryID: SelectedVideoCategory.all.name, categoryData: foruthIndexVideoData)

            allVideoData.append(categoryDataAll)

            self?.allVideoData = allVideoData

            self?.mergeCategories(category1: self?.allVideoData[2].categoryData, category2: self?.allVideoData[1].categoryData, categoryID: .category1_2)
            self?.mergeCategories(category1: self?.allVideoData[1].categoryData, category2: self?.allVideoData[0].categoryData, categoryID: .category2_3)
            self?.mergeCategories(category1: self?.allVideoData[0].categoryData, category2: self?.allVideoData[2].categoryData, categoryID: .category3_1)

            self?.checkIfMessageExists()
        }
    }

    func updateComment(_ comment : String){
        if isValidVideoCounter {
            firebaseManager.updateComment(comment, category: category, videoID: videoID)
        }
    }

    func checkIfMessageExists() {
        if isValidVideoCounter && !isNoMessages {
            firebaseManager.checkIfMessageExists(messageID: messageID, category: category, videoID: videoID) { [weak self] exist in
                self?.isMessageIsMine = exist
            }
        } else {
            isMessageIsMine = false
        }
    }

    func deleteComment() {
        if isValidVideoCounter && !isNoMessages {
            firebaseManager.deleteComment(messageID: messageID, category: category, videoID: videoID)
        }
    }

    func checkIfLikeExists() {
        if isValidVideoCounter {
            firebaseManager.checkIfLikeExists(category: category, videoID: videoID) { [weak self] exists in
                self?.isLikeExist = exists
            }
        }
    }

    func updateUserLikes(completion: @escaping()->(Void)) {
        if isValidVideoCounter {
            firebaseManager.updateUserLike(category: category, videoID: videoID) { [weak self] isLiked in
                self?.isLikeExist = isLiked

                completion()
            }
        }
    }

    private func mergeCategories(category1 : [RemoteVideoData]?, category2: [RemoteVideoData]?, categoryID: SelectedVideoCategory) {
        guard let category1 = category1, let category2 = category2 else {
            return
        }

        var tempArray = [RemoteVideoData]()
        tempArray.append(contentsOf: category1)
        tempArray.append(contentsOf: category2)

        let data = CategoryData(categoryID: categoryID.name, categoryData: tempArray)
        self.allVideoData.append(data)
    }
}
 
