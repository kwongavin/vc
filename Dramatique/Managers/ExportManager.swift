import AssetsLibrary
import AVKit

final class ExportManager: ObservableObject {
    // MARK: - Properties

    static let shared = ExportManager()

    @Published var exportedMediaUrl: URL?
    @Published var progress: Float = 0
    @Published var isExporting = false
    @Published var shouldShowShareSheet = false

    var assetExportSession: AVAssetExportSession?
    var progressTimer: Timer?

    // MARK: - Functions

    func export(isAudioOnly: Bool, callback: ((_ isSuccess: Bool) -> Void)? = nil) {
        guard !isExporting else { return }

        progress = 0
        isExporting = true

        CompositionManager.shared.makeComposition(isSeparateCompositions: false)

        guard let composition = CompositionManager.shared.mixComposition else {
            isExporting = false
            callback?(false)

            return
        }

        guard let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }

        let filePath = documentDirectory.path + "/Dramatique Export." + (isAudioOnly ? "m4a" : "mp4")
        let savePathUrl = URL(fileURLWithPath: filePath)

        try? FileManager.default.removeItem(at: savePathUrl)

        assetExportSession = AVAssetExportSession(asset: composition, presetName: isAudioOnly ? AVAssetExportPresetAppleM4A : AVAssetExportPresetHighestQuality)

        guard let assetExportSession = self.assetExportSession else {
            isExporting = false
            callback?(false)

            return
        }

        assetExportSession.outputFileType = isAudioOnly ? .m4a : .mp4
        assetExportSession.outputURL = savePathUrl
        assetExportSession.audioMix = CompositionManager.shared.audioMix

        progressTimer = .scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(handleProgressTimer), userInfo: nil, repeats: true)

        assetExportSession.exportAsynchronously {
            DispatchQueue.main.async {
                self.progressTimer?.invalidate()
                self.progress = 1

                switch assetExportSession.status {
                case .completed:
                    self.exportedMediaUrl = savePathUrl

                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { self.isExporting = false }
                default:
                    self.isExporting = false
                }

                callback?(assetExportSession.status == .completed)
            }
        }
    }

    @objc private func handleProgressTimer() {
        progress = assetExportSession?.progress ?? 0
    }
}
