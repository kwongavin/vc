import Foundation

final class UndoManager: ObservableObject {
    static let shared = UndoManager()

    @Published var undoHistory = [UndoableAction]()
    @Published var undoPosition = -1
    @Published var actionId = 0
    @Published var didUndoRecently = false
    @Published var didRedoRecently = false

    func undo() {
        undoPosition -= 1
        updateAfterChangingHistory(isUndo: true)
    }

    func redo() {
        undoPosition += 1
        updateAfterChangingHistory(isUndo: false)
    }

    func addToHistory(_ action: UndoableAction) {
        didUndoRecently = false
        didRedoRecently = false

        if undoPosition != undoHistory.count - 1 {
            if undoPosition == -1 {
                undoHistory.removeAll()
            } else {
                undoHistory.removeLast(undoHistory.count - undoPosition - 1)
            }
        }

        undoHistory.append(action)

        undoPosition += 1
    }

    private func updateAfterChangingHistory(isUndo: Bool) {
        actionId += 1
        let actionId = self.actionId

        didUndoRecently = isUndo
        didRedoRecently = !isUndo

        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
            guard let `self` = self, actionId == self.actionId else { return }

            self.didUndoRecently = false
            self.didRedoRecently = false
        }

        SampleManager.shared.recentlySelectedSample = nil
        RecordingManager.shared.didStopRecordingRecently = false

        let history: [UndoableAction] = undoPosition < 0 ? [] : Array(undoHistory[0 ... undoPosition])

        let recordedSamplesAction = history.last { element in
            guard case .recordedSamples = element else { return false }

            return true
        }

        if case let .recordedSamples(recordedSamples) = recordedSamplesAction, RecordingManager.shared.recordedSamples != recordedSamples {
            RecordingManager.shared.recordedSamples = recordedSamples
        } else {
            RecordingManager.shared.resetRecordedSamples()
        }

        let selectedSamplesAction = history.last { element in
            guard case .selectedSamples = element else { return false }

            return true
        }

        if case let .selectedSamples(selectedSamples, _) = selectedSamplesAction {
            SampleManager.shared.selectedSamples = selectedSamples
        } else {
            SampleManager.shared.reset()
        }

        VideoManager.shared.load()
    }

    func reset() {
        undoHistory.removeAll()
        undoPosition = -1
    }
}
