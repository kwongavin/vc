import AVFoundation

final class VideoManager: ObservableObject {
    static let shared = VideoManager()

    @Published var url: URL?
    @Published var duration: Double = 0
    @Published var currentTime: Double = 0
    @Published var isPlaying = false
    @Published var didStopRecently = false
    @Published var playbackId = 0

    func load() {
        CompositionManager.shared.makeComposition(isRecording: RecordingManager.shared.isCountingDown)
        
        NotificationCenter.default.post(name: NotificationName.loadVideo.name, object: nil)
    }

    func togglePlayback(isVideoPlaybackHandled: Bool = false) {
        guard url != nil else { return }

        playbackId += 1

        if !isVideoPlaybackHandled {
            NotificationCenter.default.post(name: NotificationName.toggleVideoPlayback.name, object: nil)
        }

        isPlaying.toggle()

        if !isPlaying {
            let playbackId = self.playbackId

            didStopRecently = true

            DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
                guard let `self` = self, self.playbackId == playbackId else { return }

                self.didStopRecently = false
            }
        }
    }

    func reset() {
        url = nil
        duration = 0
        currentTime = 0
        isPlaying = false
    }
}
