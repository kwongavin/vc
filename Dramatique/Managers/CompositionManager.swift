import AVFoundation

final class CompositionManager: ObservableObject {
    // MARK: - Properties

    static let shared = CompositionManager()

    var originalVideoComposition: AVMutableComposition?
    var samplesComposition: AVMutableComposition?
    var mixComposition: AVMutableComposition?

    var mixInputParametersArray = [AVMutableAudioMixInputParameters]()
    var audioMix: AVMutableAudioMix?

    // MARK: - Functions

    func makeComposition(isRecording: Bool = false, isSeparateCompositions: Bool = true) {
        if isSeparateCompositions {
            originalVideoComposition = .init()
            samplesComposition = .init()
        } else {
            mixComposition = .init()
        }

        mixInputParametersArray.removeAll()

        let composition = isSeparateCompositions ? originalVideoComposition : mixComposition

        guard let videoUrl = VideoManager.shared.url else { return }

        let videoAsset = AVAsset(url: videoUrl)
        let videoAssetTrack = videoAsset.tracks(withMediaType: .video)[0]

        guard let videoTrack = composition?.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid) else { return }

        videoTrack.preferredTransform = videoAssetTrack.preferredTransform

        do {
            try videoTrack.insertTimeRange( .init( start: .zero, duration: videoAssetTrack.timeRange.duration), of: videoAssetTrack, at: .zero)
        } catch {
            print("Error inserting time range to the video track:", error)
        }

        handleAudioOfVideoTrack(videoAsset: videoAsset, videoAssetTrack: videoAssetTrack, isRecording: isRecording, isSeparateCompositions: isSeparateCompositions )

        guard !isRecording else { return }

        handleSampleTracks(isSeparateCompositions: isSeparateCompositions)

        audioMix = .init()
        audioMix?.inputParameters = mixInputParametersArray
    }

    private func handleAudioOfVideoTrack(videoAsset: AVAsset, videoAssetTrack: AVAssetTrack, isRecording: Bool, isSeparateCompositions: Bool) {
        let composition = isSeparateCompositions ? originalVideoComposition : mixComposition

        let audioOfVideoTrackOrNil = composition?.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)

        if !isSeparateCompositions {
            let mixInputParameters = AVMutableAudioMixInputParameters(track: audioOfVideoTrackOrNil)

            if let trackID = audioOfVideoTrackOrNil?.trackID {
                mixInputParameters.trackID = trackID
            }

            mixInputParameters.setVolume(Float(AppManager.shared.originalVolume), at: .zero)
            mixInputParametersArray.append(mixInputParameters)
        }

        guard let audioOfVideoAssetTrack = videoAsset.tracks(withMediaType: .audio).first,
                let audioOfVideoTrack = audioOfVideoTrackOrNil else { return }

        if AppManager.shared.isSoloModeOn,
            !isRecording,
            !RecordingManager.shared.recordedSamples.flatMap({ $0 }).isEmpty
        {
            handleAudioOfVideoTrackSoloMode(audioOfVideoTrack: audioOfVideoTrack, audioOfVideoAssetTrack: audioOfVideoAssetTrack)
        } else {
            do {
                try audioOfVideoTrack.insertTimeRange( .init(start: .zero, duration: videoAssetTrack.timeRange.duration), of: audioOfVideoAssetTrack, at: .zero)
            } catch {
                print("Error inserting time range to the audio track of the video:", error)
            }
        }
    }

    func handleAudioOfVideoTrackSoloMode(audioOfVideoTrack: AVMutableCompositionTrack, audioOfVideoAssetTrack: AVAssetTrack) {
        let recordings = RecordingManager.shared.recordedSamples.flatMap { $0 }
        let ranges = recordings.map { $0.start ... ($0.end ?? 0) }
        let mergedRanges = merge(ranges)
        let timescale = CMTimeScale(SoundManager.shared.bitrate)

        for index in 0 ..< mergedRanges.count {
            do {
                let rangeStart = index == 0 ? 0 : mergedRanges[index - 1].upperBound
                let startTime = CMTime(seconds: rangeStart, preferredTimescale: timescale)
                let durationTime = CMTime(seconds: mergedRanges[index].lowerBound - rangeStart, preferredTimescale: timescale)
                let timeRange = CMTimeRange(start: startTime, duration: durationTime)
                let atTime = CMTime(seconds: rangeStart, preferredTimescale: timescale)

                try audioOfVideoTrack.insertTimeRange(timeRange, of: audioOfVideoAssetTrack, at: atTime)
            } catch {
                print("Error inserting time range to the audio track of the video:", error)
            }

            let startTime = CMTime(seconds: mergedRanges[index].lowerBound, preferredTimescale: .init(SoundManager.shared.bitrate))
            let durationTime = CMTime(seconds: mergedRanges[index].upperBound - mergedRanges[index].lowerBound, preferredTimescale: timescale)
            let timeRange = CMTimeRange(start: startTime, duration: durationTime)

            audioOfVideoTrack.insertEmptyTimeRange(timeRange)

            if index == mergedRanges.count - 1 {
                do {
                    let durationTime = CMTime(seconds: VideoManager.shared.duration - mergedRanges[index].upperBound, preferredTimescale: timescale)
                    let timeRange = CMTimeRange(start: .init(seconds: mergedRanges[index].upperBound, preferredTimescale: timescale), duration: durationTime)
                    let atTime = CMTime(seconds: mergedRanges[index].upperBound, preferredTimescale: timescale)

                    try audioOfVideoTrack.insertTimeRange(timeRange, of: audioOfVideoAssetTrack, at: atTime)
                } catch {
                    print("Error inserting time range to the audio track of the video:", error)
                }
            }
        }
    }

    // github.com/diwu/LeetCode-Solutions-in-Swift/blob/master/Solutions/Solutions/Hard/Hard_056_Merge_Intervals.swift
    private func merge(_ ranges: [ClosedRange<Double>]) -> [ClosedRange<Double>] {
        guard ranges.count > 1 else { return ranges }

        let ranges = ranges.sorted { $0.lowerBound <= $1.lowerBound }
        var results = [ClosedRange<Double>]()
        var start = ranges[0].lowerBound
        var end = ranges[0].upperBound

        for range in ranges {
            if range.lowerBound <= end {
                end = max(end, range.upperBound)
            } else {
                results.append(start ... end)
                start = range.lowerBound
                end = range.upperBound
            }
        }

        results.append(start ... end)

        return results
    }

    private func handleSampleTracks(isSeparateCompositions: Bool) {
        let sampleStartUrls = SampleManager.shared.selectedSampleStartUrls

        let sampleStartAssets = sampleStartUrls.map { element -> AVAsset? in
            guard let url = element else {
                return nil
            }

            return AVAsset(url: url)
        }

        let sampleStartAssetTracks = sampleStartAssets.map { $0?.tracks(withMediaType: .audio).first }

        let sampleLoopUrls = SampleManager.shared.selectedSampleLoopUrls

        let sampleLoopAssets = sampleLoopUrls.map { element -> AVAsset? in
            guard let url = element else { return nil }

            return AVAsset(url: url)
        }

        let sampleLoopAssetTracks = sampleLoopAssets.map { $0?.tracks(withMediaType: .audio).first }

        insertSampleTimeRanges(composition: isSeparateCompositions ? samplesComposition : mixComposition, sampleStartAssetTracks: sampleStartAssetTracks, sampleLoopAssetTracks: sampleLoopAssetTracks, isSeparateCompositions: isSeparateCompositions)
    }

    private func insertSampleTimeRanges(composition: AVMutableComposition?, sampleStartAssetTracks: [AVAssetTrack?], sampleLoopAssetTracks: [AVAssetTrack?], isSeparateCompositions: Bool) {
        let recordedSamples = RecordingManager.shared.recordedSamples.map { element in
            element.sorted { $0.start < $1.start }
        }

        let selectedSamples = SampleManager.shared.selectedSamples

        var recordingsForTracks = [AVMutableCompositionTrack: [Recording]]()
        var mixInputParametersForTracks = [AVMutableCompositionTrack: AVMutableAudioMixInputParameters]()

        for trackIndex in 0 ..< recordedSamples.count {
            for recording in recordedSamples[trackIndex] {
                guard let sample = selectedSamples[trackIndex],
                      let startAssetTrack = sampleStartAssetTracks[trackIndex] else { continue }

                let lastRecordingsForTracks = recordingsForTracks.compactMapValues { $0.last }

                let recordingsForTrackToUse = lastRecordingsForTracks.first { element in
                    let releaseInSeconds = Double(sample.release) / SoundManager.shared.bitrate
                    let endWithRelease = (element.value.end ?? 0) + releaseInSeconds

                    return endWithRelease < recording.start
                }

                var track = recordingsForTrackToUse?.key

                if track == nil {
                    track = composition?.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)

                    guard let track = track else { return }

                    let mixInputParameters = AVMutableAudioMixInputParameters(track: track)
                    mixInputParameters.trackID = track.trackID

                    recordingsForTracks[track] = []
                    mixInputParametersForTracks[track] = mixInputParameters
                }

                guard let track = track,
                      let mixInputParameters = mixInputParametersForTracks[track] else {
                          return
                      }

                recordingsForTracks[track]?.append(recording)

                handleSampleMixInputParameters(
                    mixInputParameters: mixInputParameters,
                    sample: sample,
                    recording: recording,
                    isSeparateCompositions: isSeparateCompositions
                )

                if sample.isLoop, let loopAssetTrack = sampleLoopAssetTracks[trackIndex] {
                    insertSampleLoopTimeRange(sample: sample, track: track, startAssetTrack: startAssetTrack, loopAssetTrack: loopAssetTrack, recording: recording)
                } else {
                    insertSampleNoLoopTimeRange(sample: sample, track: track, startAssetTrack: startAssetTrack, recording: recording)
                }
            }
        }

        if isSeparateCompositions {
            insertSampleEmptyTimeRange(composition)
        }
    }

    func handleSampleMixInputParameters(mixInputParameters: AVMutableAudioMixInputParameters, sample: Sample, recording: Recording, isSeparateCompositions: Bool) {
        let attackEndVolume = handleSampleMixInputParametersAttack(mixInputParameters: mixInputParameters, sample: sample, recording: recording, isSeparateCompositions: isSeparateCompositions)

        var decayEndVolume: Float = 0

        if recording.duration > (Double(sample.attack) / SoundManager.shared.bitrate) {
            decayEndVolume = handleSampleMixInputParametersDecay(mixInputParameters: mixInputParameters, sample: sample, recording: recording, isSeparateCompositions: isSeparateCompositions, attackEndVolume: attackEndVolume)
        }

        handleSampleMixInputParametersRelease(mixInputParameters: mixInputParameters, sample: sample, recording: recording, isSeparateCompositions: isSeparateCompositions, attackEndVolume: attackEndVolume, decayEndVolume: decayEndVolume)

        mixInputParametersArray.append(mixInputParameters)
    }

    private func handleSampleMixInputParametersAttack(mixInputParameters: AVMutableAudioMixInputParameters, sample: Sample, recording: Recording, isSeparateCompositions: Bool) -> Float {
        let attackInSeconds = Double(sample.attack) / SoundManager.shared.bitrate

        let attackEndVolume: Float = {
            var finalVolume = isSeparateCompositions ? 1 : Float(AppManager.shared.sampleVolume)

            if recording.duration < attackInSeconds {
                finalVolume *= Float(recording.duration / attackInSeconds)
            }

            return finalVolume
        }()

        let attackStart = CMTime(seconds: recording.start, preferredTimescale: Int32(SoundManager.shared.bitrate))
        let attackDuration = CMTime(seconds: min(recording.duration, attackInSeconds), preferredTimescale: Int32(SoundManager.shared.bitrate))

        mixInputParameters.setVolumeRamp(fromStartVolume: 0, toEndVolume: attackEndVolume, timeRange: .init(start: attackStart, duration: attackDuration))

        return attackEndVolume
    }

    private func handleSampleMixInputParametersDecay(mixInputParameters: AVMutableAudioMixInputParameters, sample: Sample, recording: Recording, isSeparateCompositions: Bool, attackEndVolume: Float) -> Float {
        let attackInSeconds = Double(sample.attack) / SoundManager.shared.bitrate
        let decayInSeconds = Double(sample.decay) / SoundManager.shared.bitrate

        let decayEndVolume: Float = {
            var finalVolume = Float(sample.sustain)

            if !isSeparateCompositions {
                finalVolume *= Float(AppManager.shared.sampleVolume)
            }

            if recording.duration < (attackInSeconds + decayInSeconds) {
                var sustainDelta = 1 - Float(sample.sustain)

                sustainDelta *= Float((recording.duration - attackInSeconds) / decayInSeconds)

                if !isSeparateCompositions {
                    sustainDelta *= Float(AppManager.shared.sampleVolume)
                }

                finalVolume -= sustainDelta
            }

            return finalVolume
        }()

        let decayStart = CMTime(seconds: recording.start + attackInSeconds, preferredTimescale: Int32(SoundManager.shared.bitrate))
        let decayDuration = CMTime(seconds: min(recording.duration - attackInSeconds, decayInSeconds), preferredTimescale: Int32(SoundManager.shared.bitrate))

        mixInputParameters.setVolumeRamp(fromStartVolume: attackEndVolume, toEndVolume: decayEndVolume, timeRange: .init(start: decayStart, duration: decayDuration))

        return decayEndVolume
    }

    private func handleSampleMixInputParametersRelease(mixInputParameters: AVMutableAudioMixInputParameters, sample: Sample, recording: Recording, isSeparateCompositions: Bool, attackEndVolume: Float, decayEndVolume: Float) {
        let attackInSeconds = Double(sample.attack) / SoundManager.shared.bitrate
        let decayInSeconds = Double(sample.decay) / SoundManager.shared.bitrate

        let startVolume: Float = {
            if recording.duration < attackInSeconds {
                return attackEndVolume
            } else if recording.duration < (attackInSeconds + decayInSeconds) {
                return decayEndVolume
            }

            var volume = Float(sample.sustain)

            if !isSeparateCompositions {
                volume *= Float(AppManager.shared.sampleVolume)
            }

            return volume
        }()

        let releaseStart = CMTime(seconds: recording.end ?? 0, preferredTimescale: Int32(SoundManager.shared.bitrate))
        let releaseDuration = CMTime(value: Int64(sample.release), timescale: Int32(SoundManager.shared.bitrate))

        mixInputParameters.setVolumeRamp(fromStartVolume: startVolume, toEndVolume: 0, timeRange: .init(start: releaseStart, duration: releaseDuration))
    }

    private func insertSampleLoopTimeRange(sample: Sample, track: AVMutableCompositionTrack,startAssetTrack: AVAssetTrack, loopAssetTrack: AVAssetTrack, recording: Recording) {
        let bitrate = SoundManager.shared.bitrate
        let startDurationInSeconds = Double(sample.startDuration) / Double(bitrate)

        do {
            let seconds = min(Double(sample.startDuration) / Double(bitrate), recording.duration + Double(sample.release) / Double(bitrate))
            let durationTime = CMTime(seconds: seconds, preferredTimescale: CMTimeScale(bitrate))
            let timeRange = CMTimeRange(start: .zero, duration: durationTime)
            let atTime = CMTime(seconds: recording.start, preferredTimescale: CMTimeScale(bitrate))

            try track.insertTimeRange(timeRange, of: startAssetTrack, at: atTime)

            if recording.duration > (Double(sample.startDuration - sample.release) / Double(bitrate)), let loopDuration = loopAssetTrack.asset?.duration {
                let releaseInSeconds = Double(sample.release) / .init(bitrate)
                let allLoopsDuration = recording.duration - startDurationInSeconds + releaseInSeconds
                let loopCount = Int(ceil(allLoopsDuration / loopDuration.seconds))

                for index in 0 ..< loopCount {
                    let duration: Double = {
                        if index == loopCount - 1 {
                            return allLoopsDuration - loopDuration.seconds * .init(loopCount - 1)
                        }

                        return loopDuration.seconds
                    }()

                    let start: Double = {
                        let loopingStartPoint = recording.start + startDurationInSeconds

                        return loopingStartPoint + loopDuration.seconds * Double(index)
                    }()

                    let durationTime = CMTime(seconds: duration, preferredTimescale: CMTimeScale(bitrate))
                    let timeRange = CMTimeRange(start: .zero, duration: durationTime)
                    let atTime = CMTime(seconds: start, preferredTimescale: CMTimeScale(bitrate))

                    try track.insertTimeRange(timeRange, of: loopAssetTrack, at: atTime)
                }
            }
        } catch {
            print("Error inserting loop time range to a sample track:", error)
        }
    }

    private func insertSampleNoLoopTimeRange(sample: Sample, track: AVMutableCompositionTrack, startAssetTrack: AVAssetTrack, recording: Recording) {
        let bitrate = SoundManager.shared.bitrate

        do {
            let durationTime = CMTime(seconds: recording.duration + Double(sample.release) / Double(bitrate), preferredTimescale: CMTimeScale(bitrate))
            let atTime = CMTime(seconds: recording.start, preferredTimescale: CMTimeScale(bitrate))
            let timeRange = CMTimeRange(start: .zero, duration: durationTime)

            try track.insertTimeRange(timeRange, of: startAssetTrack, at: atTime)
        } catch {
            print("Error inserting no loop time range to a sample track:", error)
        }
    }

    /// Make an empty track so that the duration of the sample composition is the same
    /// as the duration of the original video
    func insertSampleEmptyTimeRange(_ composition: AVMutableComposition?) {
        guard let silenceUrl = Bundle.main.url(forResource: "silence", withExtension: "wav") else { return }

        let silenceAsset = AVAsset(url: silenceUrl)
        let silenceAssetTrack = silenceAsset.tracks(withMediaType: .audio)[0]

        let bitrate = SoundManager.shared.bitrate
        let emptyTrack = composition?.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)

        do {
            let atTime = CMTime(value: Int64(VideoManager.shared.duration * bitrate - 1), timescale: CMTimeScale(bitrate))
            let durationTime = CMTime(value: 1, timescale: CMTimeScale(bitrate))

            try emptyTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: durationTime), of: silenceAssetTrack, at: atTime)
        } catch {
            print("Error inserting time range to an empty sample track:", error)
        }
    }

    func reset() {
        originalVideoComposition = nil
        samplesComposition = nil
        mixComposition = nil
    }
}
