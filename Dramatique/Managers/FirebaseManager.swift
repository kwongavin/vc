//
//  FirebaseDataManager.swift
//  Dramatique
//
//  Created by Deepak Verma on 14/04/22.
//

import Foundation
import FirebaseDatabase

final class FirebaseManager: ObservableObject {

    private let uuid = UIDevice().identifierForVendor?.uuidString ??  "5591E543-ABEC-4DD5-8684-16F7F2F1F5BA"
    var ref = Database.database().reference()

    // MARK: - Functions

    func updateComment(_ comment: String, category: String, videoID: String) {
        let path = "data/\(category)/\(videoID)/videoComments"

        ref.child(path).observeSingleEvent(of: .value, with: { [unowned self] (snapshot) in
            let timestamp = Date().currentTimeMillis()

            ref.child(path).child(uuid).updateChildValues(["\(timestamp)" : comment])
        })
    }

    func checkIfMessageExists(messageID: String, category: String, videoID: String, completion: @escaping(_ exist: Bool) -> Void) {
        let path = "data/\(category)/\(videoID)/videoComments/\(uuid)"

        ref.child(path).observeSingleEvent(of: .value, with: { (snapshot) in
               completion(snapshot.hasChild(messageID))
        })
    }

    func deleteComment(messageID: String, category: String, videoID: String) {
        let path = "data/\(category)/\(videoID)/videoComments/\(uuid)"

        ref.child(path).observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
            if snapshot.hasChild(messageID){
                self?.ref.child(path).child(messageID).removeValue()
            }
        })
    }

    func checkIfLikeExists(category: String, videoID: String, completion: @escaping(_ exists: Bool) -> Void) {
        let path = "data/\(category)/\(videoID)/videoLikes"
        guard let uuid = UIDevice().identifierForVendor?.uuidString else  { return}

        ref.child(path).observeSingleEvent(of: .value, with: { (snapshot) in
            completion(snapshot.hasChild(uuid))
        })
    }

    func updateUserLike(category: String, videoID: String, completion: @escaping(_ isLiked: Bool) -> Void) {
        let path = "data/\(category)/\(videoID)/videoLikes"
        guard let uuid = UIDevice().identifierForVendor?.uuidString else  { return}

        ref.child(path).observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
                if !snapshot.hasChild(uuid) {
                   self?.ref.child(path).child(uuid).setValue(1)
                }

                completion(snapshot.hasChild(uuid))
        })
    }

    func fetchRemoteVideoData(completion: @escaping(_ allVideoData: [CategoryData]) -> Void) {
        let path = "data"

        ref.child(path).observe(.value) { snapshot in
            var allVideoData = [CategoryData]()

            guard let data = snapshot.value as? [String : Any] else { return }

            var categoryData: CategoryData
            var remoteVideo = [RemoteVideoData]()
            var messageData = [MessageData]()
            var likeData = [RemoteVideoLikes]()
            var externalLink = String()
            var videoUrl = String()
            var categoryID = String()

            for (key,value) in data {
                remoteVideo.removeAll()
                likeData.removeAll()
                messageData.removeAll()
                videoUrl = ""
                externalLink = ""
                categoryID = key

                guard let data1 = value as? [String : Any] else { return }

                for (key, value) in data1 {
                    guard let data2 = value as? [String : Any] else { return }

                    for (key,value) in data2 {
                         switch TypeOFVideoData(rawValue: key) {
                         case .videoUrl :
                             guard let url = value as? String else { return }

                             videoUrl = url
                         case .videoComments :
                                 guard let messages = value as? [String : Any] else { return }

                                 for (_,value) in messages {
                                     guard let messageValue = value as? [String : String] else { return }

                                     for (key,value) in messageValue {
                                         messageData.append(MessageData(messageId: key, message: value))
                                     }
                                 }
                         case .videoLikes :
                                guard let likesData = value as? [String : Any] else { return }

                                for (key,value) in likesData {
                                    if let likeValue = value as? Int {
                                        likeData.append(RemoteVideoLikes(likeId: key, likes: likeValue))
                                    }
                                }
                         case .none:
                                 print("None")
                         case .description:
                             guard let value = value as? String else { return }

                             if !messageData.isEmpty {
                                 messageData.insert(MessageData(messageId: key, message: value), at: 0)
                             } else {
                                 messageData.append(MessageData(messageId: key, message: value))
                             }
                         case .externalLink:
                             guard let url = value as? String else { return }

                             externalLink = url
                         }
                    }

                    messageData = messageData.sorted { $0.messageId > $1.messageId }

                    remoteVideo.append(RemoteVideoData(categoryID: categoryID, videoId: key, videoUrl: videoUrl, externalLink: externalLink, messageData: messageData, videoLike: likeData))
                    messageData.removeAll()
                    likeData.removeAll()
               }

                remoteVideo = remoteVideo.sorted { remoteVideo, remoteVideo1 in
                    let value = remoteVideo.videoId.replacingOccurrences(of: "videoData", with: "")
                    let value1 = remoteVideo1.videoId.replacingOccurrences(of: "videoData", with: "")
                    return value >= value1
                }

                categoryData = CategoryData(categoryID: key, categoryData: remoteVideo)
                allVideoData.append(categoryData)
            }

            completion(allVideoData)
        }
    }
}
