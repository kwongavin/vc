import AVFoundation

final class SampleManager: ObservableObject {
    // MARK: - Properties

    static let shared = SampleManager()

    private let appManager = AppManager.shared
    static let sampleCount = 3

    @Published var selectedSamples: [Sample?] = Array(
        repeating: nil,
        count: SampleManager.sampleCount
    )

    @Published var sampleToPreview: Sample?
    @Published var shouldMuteOriginal = false
    @Published var selectedSampleIndex: Int?
    @Published var recentlySelectedSample: Sample?
    @Published var sampleSelectionId = 0
    @Published var shouldShowSampleSelection = false
    @Published var shouldShowSampleOverrideAlert = false

    @Published var isTouchedDown = Array(repeating: false, count: SampleManager.sampleCount) {
        didSet {
            isTouchedDownDidSet()
        }
    }

    var selectedSampleStartUrls: [URL?] {
        selectedSamples.map { $0?.startUrl }
    }

    var selectedSampleLoopUrls: [URL?] {
        selectedSamples.map { $0?.startUrl }
    }

    func reset() {
        selectedSamples = .init(repeating: nil, count: SampleManager.sampleCount)
        recentlySelectedSample = nil
        selectedSampleIndex = nil
    }

    private func isTouchedDownDidSet() {
        guard appManager.isSoloModeOn else {
            return
        }

        let shouldMute = !isTouchedDown.filter { $0 }.isEmpty

        if shouldMute && !shouldMuteOriginal {
            NotificationCenter.default.post(name: NotificationName.changeOriginalVolume.name, object: nil, userInfo: [UserInfoConstants.volume: Float(0)])
            NotificationCenter.default.post(name: NotificationName.ignoreOriginalVolumeRequests.name, object: nil)
        } else if !shouldMute && shouldMuteOriginal {
            NotificationCenter.default.post(name: NotificationName.stopIgnoringOriginalVolumeRequests.name, object: nil)
            NotificationCenter.default.post(name: NotificationName.changeOriginalVolume.name, object: nil, userInfo: [UserInfoConstants.volume: Float(appManager.originalVolume)])
        }

        shouldMuteOriginal = shouldMute
    }
}
