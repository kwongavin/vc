//
//  NetworkConnectionManager.swift
//  Dramatique
//
//  Created by Difeng Chen on 6/5/22.
//

import Foundation
import Reachability

final class NetworkConnectionManager {

    var connection: Reachability.Connection {
        (try? Reachability().connection) ?? .unavailable
    }
}
