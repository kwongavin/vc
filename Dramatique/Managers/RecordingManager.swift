import AVFoundation

final class RecordingManager: ObservableObject {
    // MARK: - Properties

    static let shared = RecordingManager()

    var recordingTimer: Timer?

    @Published var isCountingDown = false
    @Published var countdown = 3
    @Published var recordedSamples: [[Recording]] = Array(repeating: [], count: SampleManager.sampleCount)
    @Published var isRecording = false
    @Published var didStopRecordingRecently = false
    @Published var recordingStartTime: Double?
    @Published var recordingEndTime: Double?
    @Published var shouldShowRecordAlert = false

    // MARK: - Functions

    func toggleRecording() {
        guard VideoManager.shared.url != nil else { return }

        NotificationCenter.default.post(name: NotificationName.pauseVideo.name, object: nil)
        VideoManager.shared.isPlaying = false

        if isCountingDown || isRecording {
            stopRecording()
        } else {
            startRecording()
        }
    }

    private func startRecording() {
        recordingEndTime = nil
        
        if VideoManager.shared.duration - VideoManager.shared.currentTime < 0.1 {
            NotificationCenter.default.post(name: NotificationName.seekToStartOfVideo.name, object: nil)
            recordingStartTime = 0
        } else {
            recordingStartTime = VideoManager.shared.currentTime
        }

        isCountingDown = true

        VideoManager.shared.load()

        recordingTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(handleRecordingTimer), userInfo: nil, repeats: true)
    }

    private func stopRecording() {
        recordingTimer?.invalidate()

        let wasRecording = isRecording

        recordingStartTime = nil
        recordingEndTime = VideoManager.shared.currentTime

        countdown = 3
        isCountingDown = false
        isRecording = false

        if wasRecording {
            handleNewRecording()
        }

        VideoManager.shared.load()

        didStopRecordingRecently = true

        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { self.didStopRecordingRecently = false }
    }

    private func handleNewRecording() {
        let currentTime = VideoManager.shared.currentTime

        for sampleRowIndex in 0 ..< recordedSamples.count {
            for recording in recordedSamples[sampleRowIndex] {
                let recordingStart = min(recordingStartTime ?? 0, currentTime)
                let recordingRange = recordingStart ... currentTime

                if recording.isNew {
                    if recording.end == nil {
                        recording.end = currentTime
                    }

                    recording.isNew = false
                } else if let recordingEnd = recording.end,
                          recordingRange.overlaps(recording.start ... recordingEnd),
                          let index = recordedSamples[sampleRowIndex].firstIndex(of: recording)
                {
                    recordedSamples[sampleRowIndex].remove(at: index)
                }
            }
        }

        UndoManager.shared.addToHistory(.recordedSamples(recordedSamples))
    }

    @objc private func handleRecordingTimer() {
        countdown -= 1

        guard countdown == 0 else { return }

        recordingTimer?.invalidate()
        recordingTimer = nil

        countdown = 3
        isCountingDown = false
        isRecording = true

        NotificationCenter.default.post(name: NotificationName.playVideo.name, object: nil)
    }

    func reset() {
        resetRecordedSamples()
    }

    func resetRecordedSamples() {
        recordedSamples = Array(repeating: [], count: SampleManager.sampleCount)
    }
}
