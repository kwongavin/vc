//
//  ControlUndoRedoButtonViewModel.swift
//  Dramatique
//
//  Created by Difeng Chen on 6/12/22.
//

import Foundation

final class ControlUndoRedoButtonViewModel {
    private let recordingManager = RecordingManager.shared
    private let videoManager = VideoManager.shared
    private let appManager = AppManager.shared
    private let undoManager = UndoManager.shared

    var isUndo = false

    var isDisabled: Bool {
        if videoManager.url == nil {
            return false
        }

        let isRecordingOrCountingDown: Bool = {
            recordingManager.isRecording || recordingManager.isCountingDown
        }()

        let shouldBeDisabledBasedOnHistory: Bool = {
            if appManager.shouldShowRenderSettings {
                return false
            }

            if isUndo {
                return undoManager.undoPosition == -1
            }

            let isLastPosition = undoManager.undoPosition == undoManager.undoHistory.count - 1
            let isHistoryEmpty = undoManager.undoHistory.isEmpty

            return isLastPosition || isHistoryEmpty
        }()

        return isRecordingOrCountingDown || videoManager.isPlaying || shouldBeDisabledBasedOnHistory
    }
}
