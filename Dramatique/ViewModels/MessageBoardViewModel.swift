//
//  MessageBoardViewModel.swift
//  Dramatique
//
//  Created by Difeng Chen on 6/7/22.
//

import UIKit
import Combine

final class MessageBoardViewModel: ObservableObject {
    @Published var isPauseMessage = false
    @Published var countNum = 3

    private let appManager = AppManager.shared
    private let videoManager = VideoManager.shared
    private let undoManager = UndoManager.shared
    private let sampleManager = SampleManager.shared
    private let exportManager = ExportManager.shared
    private let recordingManager = RecordingManager.shared

    var title: String {
        if exportManager.isExporting {
            return exportingTitle
        } else if appManager.shouldShowRenderSettings {
            return L10n.MessageBoard.renderSettings
        } else if appManager.isChangingSampleVolume || appManager.isChangingOriginalVolume {
            return volumeTitle
        } else if recordingManager.isCountingDown {
            return countdownTitle
        } else if recordingManager.isRecording {
            return L10n.MessageBoard.recording
        } else if videoManager.url == nil && countNum == 3 && !isPauseMessage {
            return L10n.MessageBoard.welcome
        } else if videoManager.url == nil && countNum == 2 && !isPauseMessage {
            return L10n.MessageBoard.welcome2
        } else if videoManager.url == nil && countNum == 1 && !isPauseMessage {
            return L10n.MessageBoard.welcome3
        } else if videoManager.url == nil && countNum == 0 && !isPauseMessage {
            return L10n.MessageBoard.welcome4
        } else if isPauseMessage {
            return ""
        } else if videoManager.isPlaying {
            return L10n.MessageBoard.playing
        } else if undoManager.didUndoRecently || undoManager.didRedoRecently {
            return undoRedoTitle
        } else if let sample = sampleManager.recentlySelectedSample {
            return L10n.MessageBoard.sampleLoaded(sample.name)
        } else if videoManager.didStopRecently {
            return L10n.MessageBoard.playingStopped
        } else if appManager.isSoloModeOn && appManager.showSoloModeOnMessage {
            return L10n.MessageBoard.soloModeOn
        } else if !appManager.isSoloModeOn && appManager.showSoloModeOnMessage {
            return L10n.MessageBoard.soloModeOff
        } else if recordingManager.didStopRecordingRecently {
            return L10n.MessageBoard.recordingStopped
        } else if sampleManager.selectedSamples.compactMap({ $0 }).isEmpty {
            return L10n.MessageBoard.chooseSample
        } else if recordingManager.recordedSamples.flatMap({ $0 }).isEmpty {
            return L10n.MessageBoard.pressRecord
        }

        return L10n.MessageBoard.takeMoreTakes
    }

    var exportingTitle: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        let progressText = formatter.string(from: NSNumber(value: exportManager.progress)) ?? ""

        return L10n.MessageBoard.exporting(progressText)
    }

    var volumeTitle: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent

        var text = ""

        if appManager.isChangingSampleVolume {
            let value = (formatter.string(from: NSNumber(value: appManager.sampleVolume)) ?? "")
            text += L10n.MessageBoard.sampleVolume(value)
        }

        if appManager.isChangingSampleVolume && appManager.isChangingOriginalVolume {
            text += "\n"
        }

        if appManager.isChangingOriginalVolume {
            let value = (formatter.string(from: NSNumber(value: appManager.originalVolume)) ?? "")
            text += L10n.MessageBoard.originalVolume(value)
        }

        return text
    }

    var countdownTitle: String {
        let formatter = NumberFormatter()
        let countdownValue = NSNumber(value: recordingManager.countdown)

        return formatter.string(from: countdownValue) ?? ""
    }

    var undoRedoTitle: String {
        let undoRedoPrefix: String = {
            if undoManager.didUndoRecently {
                return L10n.MessageBoard.UndoRedo.undo
            }

            return L10n.MessageBoard.UndoRedo.redo
        }()

        let historyIndex: Int = {
            if undoManager.didUndoRecently {
                return undoManager.undoPosition + 1
            }

            return undoManager.undoPosition
        }()

        switch undoManager.undoHistory[historyIndex] {
        case .recordedSamples:
            return L10n.MessageBoard.UndoRedo.recording(undoRedoPrefix)
        case let .selectedSamples(selectedSamples, changeIndex):
            return L10n.MessageBoard.UndoRedo.sample(
                undoRedoPrefix,
                selectedSamples[changeIndex]?.name ?? ""
            )
        }
    }

    var fontSize: CGFloat {
        if appManager.isChangingSampleVolume && appManager.isChangingOriginalVolume,
           UIScreen.main.bounds.height <= 780 {
            return 12
        }

        return 14
    }

    // MARK: - Functions

    func onTimerReceive() {
        isPauseMessage = true

        //This .seconds(1) is interval pause. Minimum value is 1 second.
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(600), execute: { [weak self] in
            guard let `self` = self else { return }

            self.isPauseMessage = false

            if self.countNum > 0 {
                self.countNum -= 1
            } else if self.countNum == 0 {
                self.countNum = 3
            }
        })
    }
}
