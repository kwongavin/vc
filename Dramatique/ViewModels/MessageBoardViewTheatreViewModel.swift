//
//  MessageBoardViewTheatreViewModel.swift
//  Dramatique
//
//  Created by Difeng Chen on 6/12/22.
//

import Foundation

final class MessageBoardViewTheatreViewModel {
    private let appManager = AppManager.shared

    var firebaseMessages: String {
        if appManager.isExternalLink {
            if VideoManager.shared.url != nil && ((appManager.externalLinks?.categoryData.count ?? 0 > 0) && appManager.externalLinkCounter < appManager.externalLinks?.categoryData.count ?? 0) {
                return appManager.externalLinks?.categoryData[appManager.externalLinkCounter].messageData.first?.message ?? "External Link+"
            } else {
                return "Dramatique  ∙  Theater Mode"
            }

        } else if VideoManager.shared.url == nil || !(appManager.allVideoData.count > 0) || !(appManager.isValidVideoCounter)   {
            return L10n.MessageBoardTheatre.welcome
        } else  if appManager.isValidVideoCounter && appManager.messageCounter != -1 && !(appManager.isNoMessages) {
            if appManager.messageCounter < appManager.allVideoData[appManager.videoCategory.rawValue].categoryData[appManager.videoCounter].messageData.count {
                let message = appManager.allVideoData[appManager.videoCategory.rawValue].categoryData[appManager.videoCounter].messageData[appManager.messageCounter].message
                return message
            } else {
                appManager.messageCounter = appManager.messageCounter - 1
                return ""
            }
        } else {
            return L10n.MessageBoardTheatre.welcome
        }
    }
}
