//
//  SamplesViewViewModel.swift
//  Dramatique
//
//  Created by Difeng Chen on 6/12/22.
//

import Foundation

final class SamplesViewViewModel: ObservableObject {
    @Published var samples = [Sample]()
    @Published var playingSampleIndex: Int?

    private let sampleManager = SampleManager.shared
    private let soundManager = SoundManager.shared
    private let undoManager = UndoManager.shared

    func buttonAction(for index: Int) {
        if sampleManager.sampleToPreview != nil {
            soundManager.stopSound()
        }

        playingSampleIndex = index
        sampleManager.sampleToPreview = samples[index]

        soundManager.playSound()
    }

    func innerButtonAction(for index: Int) {
        guard let sampleIndex = sampleManager.selectedSampleIndex ?? nil else { return }

        soundManager.stopSound()
        sampleManager.sampleToPreview = nil

        let sample = samples[index]
        sampleManager.sampleSelectionId += 1
        sampleManager.selectedSamples[sampleIndex] = sample
        sampleManager.recentlySelectedSample = sample

        undoManager.addToHistory(
            .selectedSamples(sampleManager.selectedSamples, sampleIndex)
        )

        let sampleSelectionId = sampleManager.sampleSelectionId

        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
            if sampleSelectionId == self?.sampleManager.sampleSelectionId {
                self?.sampleManager.recentlySelectedSample = nil
            }
        }

        sampleManager.shouldShowSampleSelection = false
    }
}
