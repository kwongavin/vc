//
//  SelectedSampleViewViewModel.swift
//  Dramatique
//
//  Created by Difeng Chen on 6/12/22.
//

import SwiftUI

final class SelectedSampleViewViewModel {
    private let sampleManager = SampleManager.shared
    private let recordingManager = RecordingManager.shared
    private let videoManager = VideoManager.shared

    var index: Int = 0

    var sample: Sample? {
        sampleManager.selectedSamples[index]
    }

    var isTouchedDown: Bool {
        sampleManager.isTouchedDown[index]
    }

    func sampleAddButtonAction() {
        print("Sample Add button pressed - sample index \(index)")

        if !recordingManager.recordedSamples[index].isEmpty {
            sampleManager.selectedSampleIndex = index
            sampleManager.shouldShowSampleOverrideAlert = true
        } else {
            showSampleSelection()
        }
    }

    func showSampleSelection() {
        sampleManager.selectedSampleIndex = index
        sampleManager.shouldShowSampleSelection = true
    }
}
