//
//  DrawerBottomViewViewModel.swift
//  Dramatique
//
//  Created by Difeng Chen on 6/12/22.
//

import UIKit

final class DrawerBottomViewViewModel {
    private let appManager = AppManager.shared
    private let videoManager = VideoManager.shared
    private let recordingManager = RecordingManager.shared
    private let sampleManager = SampleManager.shared

    func reset(_ fromInitialSlides : Bool = false) {
        if videoManager.url != nil {
            appManager.shouldShowResetAlert = true
        } else {
            appManager.reset()
            if !fromInitialSlides {
                appManager.showTheatreMode.toggle()
            }

        }
        UserDefaults.standard.setValue(appManager.showTheatreMode, forKey: "showTheatreMode")
        print("test \(appManager.showTheatreMode)")
    }

    func settingsButtonAction() {
        appManager.isSoloModeOn = !appManager.isSoloModeOn
        appManager.showSoloModeOnMessage = true
        // Stop display soloMode Messages
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) { [weak self] in
            self?.appManager.showSoloModeOnMessage = false
        }

        VideoManager.shared.load()
    }

    func rightButtonAction() {
        appManager.shouldShowRenderSettings.toggle()

        if appManager.shouldShowRenderSettings {
            recordingManager.didStopRecordingRecently = false
            sampleManager.recentlySelectedSample = nil
        }

        appManager.shouldAnimateDrawer = true

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            self?.appManager.shouldAnimateDrawer = false
        }
    }

    func likeButtonAction() {
        if appManager.isExternalLink {
            if appManager.externalLinkCounter >= appManager.externalLinks?.categoryData.count ?? 0 {
                appManager.externalLinkCounter = 0
            }

            guard let externLinkData = appManager.externalLinks else { return }
            guard let url = URL(string: externLinkData.categoryData[appManager.externalLinkCounter].externalLink) else { return }

            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}
