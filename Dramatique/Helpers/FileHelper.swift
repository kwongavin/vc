import SwiftUI

final class FileHelper {
    static let shared = FileHelper()
    
    var documentDirectory: URL {
        FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
}
