import SwiftUI

struct CategoriesView: View {
    @State private var categories = [Category]()

    var body: some View {
        NavigationView {
            List {
                ForEach(categories, id: \.self) { category in
                    ZStack {
                        NavigationLink(destination: SampleView(category: category)) {
                            EmptyView()
                        }

                        HStack {
                            Text(category.name)
                                .foregroundColor(.white)

                            Spacer()

                            Image(systemName: "chevron.right")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 7)
                                .foregroundColor(.white)
                        }
                    }
                }
                    .listRowBackground(Color(.uiTableViewBackgroundDarkBlue))
            }
                .navigationBarTitle(L10n.Categories.title)
                .onAppear {
                    categories = Categories.shared.categories
                }
        }
            .preferredColorScheme(.dark)
    }
}
