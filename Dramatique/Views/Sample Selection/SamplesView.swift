import SwiftUI

struct SampleView: View {
    @StateObject private var sampleManager = SampleManager.shared
    @StateObject private var appManager = AppManager.shared
    @StateObject private var undoManager = UndoManager.shared
    @StateObject private var viewModel = SamplesViewViewModel()
    private let soundManager = SoundManager.shared

    let category: Category

    var body: some View {
        List {
            ForEach(0 ..< viewModel.samples.count, id: \.self) { index in
                buttonFor(index: index)
            }
        }
            .navigationBarTitle(category.name)
            .onAppear {
                viewModel.samples = Samples.shared.samples(category: category)
            }
            .onDisappear {
                soundManager.stopSound()
                sampleManager.sampleToPreview = nil
            }
    }

    func rowBackground(sampleIndex: Int) -> Color {
        guard sampleIndex != viewModel.playingSampleIndex else { return Color(.backgroundDarkBlue) }

        return Color(.uiTableViewBackgroundDarkBlue)
    }

    func buttonFor(index: Int) -> some View {
        Button {
            viewModel.buttonAction(for: index)
        } label: {
            HStack(spacing: 0) {
                HStack(spacing: 0) {
                    if index == viewModel.playingSampleIndex {
                        Image(Asset.sampleVolume.name)
                    }

                    Spacer()
                }
                    .frame(maxWidth: 32)

                Text(viewModel.samples[index].name)
                    .foregroundColor(.white)

                Spacer()

                if index == viewModel.playingSampleIndex {
                    Button {
                        viewModel.innerButtonAction(for: index)
                    } label: {
                        VectorImage(name: Asset.sampleAdd.name)
                            .frame(width: 18, height: 18)
                    }
                }
            }
        }
            .listRowBackground(rowBackground(sampleIndex: index))
    }
}
