import SwiftUI

struct ControlButtonStyle: ButtonStyle {
    let size: CGFloat
    let showTheatreMode: Bool

    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .buttonStyle(PlainButtonStyle())
            .frame(width: size, height: size)
            .background(showTheatreMode ? Color(.lightBackgroundDarkBlueShapeTwo) :  Color(.backgroundGreenShapeTwo))
            .cornerRadius(UIScreen.main.bounds.height > 568 ? 16 : 10)
            .opacity(configuration.isPressed ? 0.7 : 1)
    }
}
