import SwiftUI

struct VideoDisplayButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .overlay(
                Color(red: 0.5, green: 0.5, blue: 0.5)
                    .opacity(
                        VideoManager.shared.url != nil ? 0 : (configuration.isPressed ? 0.05 : 0)
                    )
            )
    }
}
