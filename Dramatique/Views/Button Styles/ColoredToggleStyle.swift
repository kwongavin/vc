import SwiftUI

struct ColoredToggleStyle: ToggleStyle {
    var label = ""
    var onColor = Color(.green)
    var offColor = Color(.systemGray5)
    var thumbColor = Color.white

    func makeBody(configuration: Self.Configuration) -> some View {
        HStack {
            Text(label)

            Spacer()

            Button {
                configuration.isOn.toggle()
            } label: {
                RoundedRectangle(cornerRadius: 16, style: .circular)
                    .fill(configuration.isOn ? onColor : offColor)
                    .frame(width: 50, height: 29)
                    .overlay(
                        Circle()
                            .fill(thumbColor)
                            .shadow(radius: 1, x: 0, y: 1)
                            .padding(1.5)
                            .offset(x: configuration.isOn ? 10 : -10))
                    .animation(Animation.easeInOut(duration: 0.3))
            }
        }
            .padding(.horizontal)
    }
}
