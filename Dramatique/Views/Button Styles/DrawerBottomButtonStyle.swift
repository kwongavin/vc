import SwiftUI

struct DrawerBottomButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .buttonStyle(PlainButtonStyle())
            .foregroundColor(.white)
            .frame(maxWidth: .infinity)
            .padding([.top, .bottom], UIScreen.main.bounds.height > 568 ? 20 : 12)
            .contentShape(Rectangle())
            .opacity(configuration.isPressed ? 0.7 : 1)
    }
}
