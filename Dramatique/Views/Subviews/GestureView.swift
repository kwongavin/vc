import SwiftUI

struct GestureView: UIViewRepresentable {
    typealias LongPressGestureCallback = (_ gesture: UILongPressGestureRecognizer) -> Void
    typealias PanGestureCallback = (_ gesture: UIPanGestureRecognizer, _ location: CGPoint) -> Void

    let longPressGestureCallback: LongPressGestureCallback
    let panGestureCallback: PanGestureCallback?

    init(longPressGestureCallback: @escaping LongPressGestureCallback, panGestureCallback: PanGestureCallback? = nil) {
        self.longPressGestureCallback = longPressGestureCallback
        self.panGestureCallback = panGestureCallback
    }

    func makeUIView(context: UIViewRepresentableContext<GestureView>) -> GestureView.UIViewType {
        let view = UIView(frame: .zero)

        let longPressGesture = UILongPressGestureRecognizer(
            target: context.coordinator,
            action: #selector(Coordinator.longPressed(_:))
        )

        longPressGesture.delegate = context.coordinator
        longPressGesture.minimumPressDuration = 0
        view.addGestureRecognizer(longPressGesture)

        if panGestureCallback != nil {
            let panGesture = UIPanGestureRecognizer(
                target: context.coordinator,
                action: #selector(Coordinator.panned(_:))
            )

            panGesture.delegate = context.coordinator
            panGesture.maximumNumberOfTouches = 1
            view.addGestureRecognizer(panGesture)
        }

        return view
    }

    class Coordinator: NSObject, UIGestureRecognizerDelegate {
        let longPressGestureCallback: LongPressGestureCallback
        let panGestureCallback: PanGestureCallback?

        init(longPressGestureCallback: @escaping LongPressGestureCallback, panGestureCallback: PanGestureCallback?) {
            self.longPressGestureCallback = longPressGestureCallback
            self.panGestureCallback = panGestureCallback
        }

        @objc fileprivate func longPressed(_ gesture: UILongPressGestureRecognizer) {
            longPressGestureCallback(gesture)
        }

        @objc fileprivate func panned(_ gesture: UIPanGestureRecognizer) {
            panGestureCallback?(gesture, gesture.location(in: gesture.view))
        }

        func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
            true
        }
    }

    func makeCoordinator() -> GestureView.Coordinator {
        .init(
            longPressGestureCallback: longPressGestureCallback,
            panGestureCallback: panGestureCallback
        )
    }

    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<GestureView>) {
    }
}
