//
//  PullToRefreshControl.swift
//  Dramatique
//
//  Created by Deepak Verma on 15/04/22.
//

import SwiftUI

struct PullToRefreshControl: View {
    var coordinateSpaceName: String
    var onRefresh: () -> Void

    @State var needRefresh = false

    var body: some View {
        GeometryReader { geo in
            if (geo.frame(in: .named(coordinateSpaceName)).midY > 20) {
                Spacer()
                    .onAppear {
                        needRefresh = true
                    }
            } else if (geo.frame(in: .named(coordinateSpaceName)).maxY < 10) {
                Spacer()
                    .onAppear {
                        if needRefresh {
                            needRefresh = false
                            onRefresh()
                        }
                    }
            }

            HStack {
                Spacer()

                if needRefresh {
                    CircularProgress()
                        .offset(x: 0, y: 10)
                } else {
                    CircularProgress()
                        .offset(x: 0, y: 10)
                }

                Spacer()
            }
        }
            .padding(.top, -50)
    }
}
