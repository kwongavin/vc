//
//  SelectedCategoryView.swift
//  Dramatique
//
//  Created by Deepak Verma on 16/04/22.
//

import SwiftUI

struct SelectedCategoryView: View {
    @StateObject private var appManager = AppManager.shared
    @StateObject private var videoManager = VideoManager.shared

    var sampleSize: CGFloat {
        return max(0, (appManager.widthWithMargin(12) - sampleSpacing * (3 - 1)) / 3)
    }

    private let sampleSpacing: CGFloat = 12
    var body: some View {
        HStack(alignment: .center, spacing: sampleSpacing) {
            CategoryView(sampleSize: sampleSize, selectedCategory: .category1, title: "Art", isCategorySelected: $appManager.categorySelected1)
            CategoryView(sampleSize: sampleSize, selectedCategory: .category2, title: "Laugh", isCategorySelected: $appManager.categorySelected2)
            CategoryView(sampleSize: sampleSize, selectedCategory: .category3, title: "Love", isCategorySelected: $appManager.categorySelected3)
        }
    }
}
