//
//  LikeButtonTheatre.swift
//  Dramatique
//
//  Created by Deepak Verma on 19/04/22.
//

import SwiftUI

struct LikeButtonTheatre: View {
    @StateObject private var appManager = AppManager.shared

    let size: CGFloat
    var iconWidth: CGFloat = 21

    var body: some View {
        Button {
            appManager.updateUserLikes {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                    appManager.checkIfLikeExists()
                }
            }
        } label: {
            ZStack {
                Image(Asset.record.name)
            }
        }
            .buttonStyle(ControlButtonStyle(size: size, showTheatreMode: appManager.showTheatreMode))
            .disableAndDim(appManager.isLikeExist)
    }
}
