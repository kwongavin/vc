//
//  CommentButton.swift
//  Dramatique
//
//  Created by Deepak Verma on 12/04/22.
//

import SwiftUI

struct CommentButton: View {
    @StateObject private var sampleManager = SampleManager.shared
    @StateObject private var appManager = AppManager.shared
    @StateObject private var videoManager = VideoManager.shared

    let size: CGFloat

    var body: some View {
        Button {
            appManager.commentButtonPressed.toggle()
        } label: {
            ZStack {
                Image(Asset.thrComment.name)
            }
        }
            .frame(width: size, height: size)
            .buttonStyle(ControlButtonStyle(size: size, showTheatreMode: appManager.showTheatreMode))
    }
}
