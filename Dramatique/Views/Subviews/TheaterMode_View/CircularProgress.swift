//
//  CircularProgress.swift
//  Dramatique
//
//  Created by Deepak Verma on 15/04/22.
//

import SwiftUI

struct CircularProgress: View {
    @State private var isLoading = false

    var body: some View {
        ZStack {
            Image(Asset.light.name)
                .rotationEffect(Angle(degrees: isLoading ? 360 : 0))
                .animation(Animation.linear(duration: 1).repeatForever(autoreverses: false))
                .onAppear {
                    self.isLoading = true
                }
        }
    }
}
