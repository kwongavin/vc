//
//  CommentView.swift
//  Dramatique
//
//  Created by Deepak Verma on 12/04/22.
//

import SwiftUI
import IQKeyboardManagerSwift

struct CommentView: View {
    @StateObject private var appManager = AppManager.shared
    @StateObject var firebase = FirebaseManager()

    @State private var comment = L10n.CommentView.placeholder
    @State private var isFirstResponder = true
    @State private var showWarning = false

    var body: some View {
        VStack {
            HStack {
                Spacer()

                HStack {
                    DramatiqueTextField(text: $comment, isFirstResponder: $isFirstResponder) {
                        $0.textColor = .white
                        $0.autocorrectionType = .yes
                        $0.spellCheckingType = .yes
                    }
                        .padding(20)
                }
                    .background(Color.gray.opacity(0.10))
                    .frame(height:40)
                    .cornerRadius(20)
                    .clipped()

                Button {
                    buttonAction()
                } label: {
                    ZStack {
                        Image(Asset.light.name)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 25, height: 25)
                            .clipped()
                            .padding(.trailing,10)
                    }
                }
                    .padding(.leading,15)
                    .background(Color.clear)
            }
        }
            .frame(width: appManager.size.width - 23, height: 35)
            .background(Color.clear.opacity(0.0))
            .offset(y: UIScreen.main.bounds.height == 667.0 ? -20 : UIScreen.main.bounds.height == 812.0 ? 20 : UIScreen.main.bounds.height ==  736.0  ? 0 : UIScreen.main.bounds.height == 896.0 ? UIScreen.main.bounds.height * 0.05 :  UIScreen.main.bounds.height == 844.0 ? UIScreen.main.bounds.height * 0.02 : UIScreen.main.bounds.height * 0.07 )
            .animation(.easeOut(duration: 0.16))
            .alert(isPresented: $showWarning) {
                Alert(title: Text(L10n.CommentView.alert), message: Text(L10n.CommentView.message), dismissButton: .default(Text(L10n.CommentView.ok), action: {
                    showWarning = false
                }))
            }
    }

    private func buttonAction() {
        if comment == L10n.CommentView.placeholder {
            showWarning = true
            return
        }

        appManager.commentButtonPressed = false
        appManager.updateComment(comment)
    }
}
