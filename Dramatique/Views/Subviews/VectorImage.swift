import SwiftUI
import UIKit

struct VectorImage: UIViewRepresentable {
    var name: String
    var contentMode: UIView.ContentMode = .scaleAspectFit
    var tintColor: UIColor = .black

    func makeUIView(context: Context) -> UIImageView {
        let imageView = UIImageView()
        imageView.setContentCompressionResistancePriority(.fittingSizeLevel, for: .vertical)

        return imageView
    }

    func updateUIView(_ imageView: UIImageView, context: Context) {
        imageView.contentMode = contentMode
        imageView.tintColor = tintColor

        if let image = UIImage(named: name) {
            imageView.image = image
        }
    }
}
