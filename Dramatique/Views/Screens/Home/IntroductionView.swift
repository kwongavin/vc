//  Created by Leon Köhler
//
// This code falls under the MIT license. Visit https://opensource.org/licenses/MIT to find out more.

import SwiftUI

struct IntroductionView: View {
    @State var currentIndex: Int = 0
    
    let timer = Timer.publish(every: 2, on: .main, in: .common).autoconnect()
    @State var showStar: Bool = false
    @State var xPos: CGFloat = 0 //random value between 0 and 1
    @State var yPos: CGFloat = 0 //random value between 0 and 1
    
    @State var transitioning: Bool = false
    @Binding var transitionToMainView: Bool
    
    @State var translation: CGSize = .zero
    @State var translationCount: Int = 0
    
    var body: some View {
        ZStack {
            Rectangle()
                .edgesIgnoringSafeArea(.all)
                .foregroundColor(Color(red: 0.157, green: 0.369, blue: 0.42))
            VStack {
                GeometryReader { geo in
                    Rectangle()
                        .foregroundColor(.clear)
                    Image("light")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 30, height: 40)
                        .scaleEffect(showStar ? 1 : 0.01)
                        .offset(x: (geo.size.width - 30) * xPos, y: (geo.size.height - 40) * yPos)
                }
                .onReceive(timer) { _ in
                    withAnimation(.default.speed(0.5)) {
                        showStar = false
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + CGFloat.random(in: 0.5..<1)) {
                        xPos = CGFloat.random(in: 0..<1)
                        yPos = CGFloat.random(in: 0..<1)
                        withAnimation(.default.speed(0.5)) {
                            showStar = true
                        }
                    }
                }
                if !transitioning {
                    ZStack {
                        RoundedRectangle(cornerRadius: 20)
                            .edgesIgnoringSafeArea(.bottom)
                            .foregroundColor(Color(red: 0.070, green: 0.086, blue: 0.141))
                        HStack {
                            VStack(spacing: 16) {
                                ZStack {
                                    Screen1()
                                    Screen2()
                                        .offset(x: UIScreen.main.bounds.width)
                                    Screen3()
                                        .offset(x: UIScreen.main.bounds.width * 2)
                                    Screen4()
                                        .offset(x: UIScreen.main.bounds.width * 3)
                                }
                                .offset(x: -UIScreen.main.bounds.width * CGFloat(currentIndex) + translation.width)
                                Spacer()
                                HStack {
                                    ForEach(0..<4, id: \.self) { i in
                                        Circle()
                                            .frame(width: currentIndex == i ? 15 : 12, height: currentIndex == i ? 15 : 12)
                                            .foregroundColor(currentIndex == i ? Color(red: 0.157, green: 0.369, blue: 0.42) : Color.gray)
                                            .onTapGesture {
                                                currentIndex = i
                                            }
                                    }
                                }
                                HStack {
                                    Button(action: {
                                        if currentIndex == 0 {
                                            withAnimation {
                                                transitioning = true
                                                UserDefaults.standard.setValue(transitioning, forKey: "onboardingCompleted")
                                                DrawerBottomViewViewModel().reset(true)

                                            }
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                                transitioning = false
                                                transitionToMainView = true
                                                UserDefaults.standard.setValue(transitionToMainView, forKey: "nextButtonClickInSlides")
                                                DrawerBottomViewViewModel().reset(true)

                                            }
                                        }
                                        else {
                                            currentIndex -= 1
                                        }
                                    }) {
                                        Text(currentIndex == 0 ? "Skip" : "Prev")
                                            .font(.headline)
                                            .bold()
                                            .frame(width: 80, height: 50)
                                    }
                                    Spacer()
                                    Button(action: {
                                        if currentIndex == 3 {
                                            //transition to main
                                            withAnimation {
                                                transitioning = true
                                            }
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                                transitioning = false
                                                transitionToMainView = true
                                            //    UserDefaults.standard.setValue(transitionToMainView, forKey: "onboardingCompleted")
                                                UserDefaults.standard.setValue(transitionToMainView, forKey: "nextButtonClickInSlides")
                                                DrawerBottomViewViewModel().reset(true)
                                            }
                                        }
                                        else {
                                            currentIndex += 1
                                        }
                                    }) {
                                        ZStack {
                                            RoundedRectangle(cornerRadius: 5)
                                                .foregroundColor(currentIndex == 3 ? Color(red: 0.157, green: 0.369, blue: 0.42) : Color.clear)
                                                .frame(width: 80, height: 40)
                                            Text(currentIndex == 3 ? "Go" : "Next")
                                                .font(.headline)
                                                .bold()
                                        }
                                    }
                                }
                            }
                            Spacer()
                        }
                        .foregroundColor(.white)
                        .padding()
                    }
                    .frame(height: 400)
                    .transition(.move(edge: .bottom))
                    .gesture(
                        DragGesture()
                            .onChanged { value in
                                if translationCount == 0 {
                                    translation = value.translation
                                    
                                    if translation.width > 100 {
                                        if currentIndex > 0 {
                                            currentIndex -= 1
                                            translationCount += 1
                                        }
                                        else {
                                           // currentIndex = 3
                                        }
                                    }
                                    else if translation.width < -100 {
                                        if currentIndex < 3 {
                                            currentIndex += 1
                                            translationCount += 1
                                        }
                                        else {
                                         //   currentIndex = 0
                                        }
                                    }
                                }
                            }
                            .onEnded { value in
                                withAnimation(.interactiveSpring()) {
                                    translation = .zero
                                    translationCount = 0
                                }
                            }
                    )
                    .animation(.default)
                }
            }
        }
    }
    
    @ViewBuilder
    func Screen1() -> some View {
        VStack(alignment: .leading, spacing: 24) {
            HStack {
                Text("Hello!")
                    .font(.system(size: 30))
                    .kerning(0.4)
                    .bold()
                Spacer()
            }
            Text("Welcome to Dramatique.\n\nThis app adds musical effects to\nmake your video your masterpiece.")
                .font(.system(size: 20))
                .fontWeight(.regular)
                .kerning(0.3)
                .minimumScaleFactor(0.8)
            Spacer()
        }
        .padding(.top, 40)
        .padding(.horizontal)
    }
    @ViewBuilder
    func Screen2() -> some View {
        VStack(alignment: .leading, spacing: 24) {
            HStack {
                Text("There are two modes")
                    .font(.system(size: 30))
                    .bold()
                Spacer()
            }
            Text("With Producer Mode,\nyou can work on your videos.\n\nWith Theater Mode,\nyou can watch featured videos.")
                .font(.system(size: 20))
                .fontWeight(.regular)
                .kerning(0.3)
                .minimumScaleFactor(0.8)
            Spacer()
        }
        .padding(.top, 40)
        .padding(.horizontal)
    }
    @ViewBuilder
    func Screen3() -> some View {
        VStack(alignment: .leading, spacing: 26) {
            HStack {
                Text("Not just a flash in the pan!")
                    .font(.system(size: 25))
                    .bold()
                Spacer()
            }
            Text("Our easy to use studio comes with a\nrefined palate of royalty free sounds,\n\nmade to breathe life and longevity\ninto your creations.")
                .font(.system(size: 20))
                .fontWeight(.regular)
                .kerning(0.3)
                .minimumScaleFactor(0.8)
            Spacer()
        }
        .padding(.top, 40)
        .padding(.horizontal)
    }
    @ViewBuilder
    func Screen4() -> some View {
        VStack(alignment: .leading, spacing: 24) {
            HStack {
                Text("#Dramatique2022")
                    .font(.system(size: 30))
                    .bold()
                Spacer()
            }
            Text("Don't forget to hashtag\n#Dramatique2022 to be featured,\n\non any SNS you like.")
                .font(.system(size: 20))
                .fontWeight(.regular)
                .kerning(0.3)
                .minimumScaleFactor(0.8)
            Spacer()
        }
        .padding(.top, 40)
        .padding(.horizontal)
    }
}

struct IntroductionView_Previews: PreviewProvider {
    static var previews: some View {
        IntroductionView(transitionToMainView: .constant(false))
    }
}
