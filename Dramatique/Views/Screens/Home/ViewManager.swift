//  Created by Leon Köhler
//
// This code falls under the MIT license. Visit https://opensource.org/licenses/MIT to find out more.


import SwiftUI

struct ViewManager: View {
    @State var transitionToMainView: Bool = UserDefaults.standard.value(forKey: "onboardingCompleted") as? Bool ?? false
    
    var body: some View {
        ZStack {
            IntroductionView(transitionToMainView: $transitionToMainView)
                .opacity(transitionToMainView ? 0 : 1)
                .onAppear {
                    AppManager.shared.showTheatreMode = UserDefaults.standard.value(forKey: "showTheatreMode") as? Bool ?? false
                }
            HomeView()
                .opacity(transitionToMainView ? 1 : 0)
        }
    }
}

struct ViewManager_Previews: PreviewProvider {
    static var previews: some View {
        ViewManager()
    }
}
