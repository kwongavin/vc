import SwiftUI

struct HomeView: View {
    @StateObject private var appManager = AppManager.shared

    init() {
        UINavigationBar.appearance().backgroundColor = .backgroundDarkBlue
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().overrideUserInterfaceStyle = .dark

        UITableView.appearance().backgroundColor = .uiTableViewBackgroundDarkBlue
        UITableView.appearance().overrideUserInterfaceStyle = .dark

        let cellBackgroundView = UIView()
        cellBackgroundView.backgroundColor = .backgroundDarkBlue
        UITableViewCell.appearance().selectedBackgroundView = cellBackgroundView
    }

    var body: some View {
        GeometryReader { metrics in
            VStack(spacing: 0) {
                TopView()
                DrawerView()
            }
                .frame(width: appManager.size.width)
                .background(appManager.showTheatreMode ? Color(.lightBackgroundDarkBlue) : Color(.backgroundGreen))
                .edgesIgnoringSafeArea(.all)
                .onAppear {
                    appManager.size = metrics.size
                    appManager.safeAreaInsets = metrics.safeAreaInsets
                }
                .onTapGesture {
                    if appManager.commentButtonPressed {
                        appManager.commentButtonPressed = false
                    }
                }
        }
            .onAppear {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    appManager.isPreparing = false
                }
            }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}


