import SwiftUI

struct RenderSettingsView: View {
    @StateObject var exportManager = ExportManager.shared
    @StateObject var sampleManager = SampleManager.shared
    @StateObject private var appManager = AppManager.shared

    var body: some View {
        VStack(spacing: 12) {
            Button {
                export(isAudioOnly: false)
            } label: {
                Text(L10n.Drawer.Bottom.renderVideo)
                    .font(.system(size: 14, weight: .semibold))
            }
                .buttonStyle(DrawerBottomButtonStyle())
                .disableAndDim(exportManager.isExporting)
                .background(
                    Color(red: 0.125, green: 0.133, blue: 0.184)
                        .disableAndDim(exportManager.isExporting)
                )
                .cornerRadius(10)

            Button {
                export(isAudioOnly: true)
            } label: {
                Text(L10n.Drawer.Bottom.renderAudioOnly)
                    .font(.system(size: 14, weight: .semibold))
            }
                .buttonStyle(DrawerBottomButtonStyle())
                .disableAndDim(exportManager.isExporting)
                .background(
                    Color(.backgroundDarkBlueShadeFour)
                        .disableAndDim(exportManager.isExporting)
                )
                .cornerRadius(10)
        }
            .frame(width: appManager.widthWithMargin(20))
            .sheet(isPresented: $exportManager.shouldShowShareSheet) {
                ActivityViewController(activityItems: [exportManager.exportedMediaUrl as Any])
                    .edgesIgnoringSafeArea(.all)
            }
    }

    func export(isAudioOnly: Bool) {
        exportManager.export(isAudioOnly: isAudioOnly) { isSuccess in
            if isSuccess {
                exportManager.shouldShowShareSheet = true
            }
        }
    }
}
