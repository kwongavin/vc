import SwiftUI

struct DrawerBottomView: View {
    @StateObject private var exportManager = ExportManager.shared
    @StateObject private var recordingManager = RecordingManager.shared
    @StateObject private var sampleManager = SampleManager.shared
    @StateObject private var appManager = AppManager.shared
    @StateObject private var videoManager = VideoManager.shared
    @StateObject private var compositionHelper = CompositionManager.shared

    @State private var shouldShowSettings = false
    @State private var shouldShowResetAlert = false

    private let bottomButtonsMargin: CGFloat = 12
    private let viewModel = DrawerBottomViewViewModel()

    var rightButtonTitle: String {
        if appManager.shouldShowRenderSettings {
            return L10n.Drawer.Bottom.cancel
        }

        return L10n.Drawer.Bottom.next
    }

    var isRightButtonDisabled: Bool {
        let isPlaying = videoManager.isPlaying
        let areNoRecordedSamples = recordingManager.recordedSamples.flatMap { $0 }.isEmpty
        let isNotRecordingOrCountingDown = !recordingManager.isRecording && !recordingManager.isCountingDown

        return (isPlaying || areNoRecordedSamples) && isNotRecordingOrCountingDown && videoManager.url != nil
    }

    var body: some View {
        VStack {
            HStack(spacing: bottomButtonsMargin) {
                if appManager.showTheatreMode && videoManager.url == nil {
                    resetButtonThetre
                } else {
                    resetButton
                }

                if appManager.showTheatreMode {
                    if appManager.isExternalLink && videoManager.url != nil {
                        bottomCommentButton.disableAndDim(true)
                        bottomLikeButton
                    } else {
                        if videoManager.url != nil {
                                bottomCommentButton
                                bottomLikeButton
                        }
                    }
                } else {
                    settingsButton.disableAndDimIfCountdownOrRecording(recordingManager: recordingManager)
                        .disableAndDimIfNoCurrentVideo(videoManager: videoManager)
                    
                    rightButton.disableAndDimIfCountdownOrRecording(recordingManager: recordingManager)
                        .disableAndDimIfNoCurrentVideo(videoManager: videoManager)
                }
            }
        }
            .frame(width: appManager.widthWithMargin(20))
            .alert(isPresented: $appManager.shouldShowResetAlert) {
                alert
            }
    }

    private var alert: Alert {
        Alert(
            title: Text(L10n.Drawer.Bottom.ResetAlert.title),
            primaryButton: .destructive(Text(L10n.Drawer.Bottom.ResetAlert.yes)) {
                appManager.categorySelected1 = true
                appManager.categorySelected2 = true
                appManager.categorySelected3 = true
                appManager.reset()
                appManager.videoCounter = 0
                appManager.messageCounter = 0
                appManager.videoCategory = .all
                appManager.videoCategorySelected = .all
            },
            secondaryButton: .cancel(Text(L10n.Drawer.Bottom.ResetAlert.no))
        )
    }

    private var resetButton: some View {
        Button {
            viewModel.reset()
        } label: {
            HStack(alignment: .center, spacing: 6) {
                Image(appManager.showTheatreMode ? Asset.thrModeBack.name : Asset.thrModeBack.name )

                Text(videoManager.url != nil ? L10n.Drawer.Bottom.reset : appManager.showTheatreMode ? L10n.Drawer.Bottom.pdMode : L10n.Drawer.Bottom.thrMode)
                    .font(.system(size: 14, weight: .bold))
            }
        }
            .buttonStyle(DrawerBottomButtonStyle())
            .disableAndDim(videoManager.isPlaying || exportManager.isExporting)
            .animation(nil)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color.white, lineWidth: 1)
                    .padding(.all, 0.5)
                    .disableAndDim(videoManager.isPlaying || exportManager.isExporting)
            )
    }

    var settingsButton: some View {
        Button {
            viewModel.settingsButtonAction()
        } label: {
            Text((appManager.isSoloModeOn) ? L10n.Drawer.Bottom.soloOn : L10n.Drawer.Bottom.soloOff)
                .font(.system(size: 14, weight: .bold))
        }
            .buttonStyle(DrawerBottomButtonStyle())
            .padding(0)
            .background( Color(red: 0.157, green: 0.369, blue: 0.42))
            .opacity((appManager.isSoloModeOn) ? 1 : 0.35)
            .cornerRadius(10)
            .disableAndDim(videoManager.isPlaying || exportManager.isExporting)
    }

    var rightButton: some View {
        Button {
            viewModel.rightButtonAction()
        } label: {
            Text(rightButtonTitle)
                    .font(.system(size: 14, weight: .bold))
        }
            .buttonStyle(DrawerBottomButtonStyle())
            .padding(0)
            .background(Color(red: 0.157, green: 0.369, blue: 0.42))
            .cornerRadius(10)
            .disableAndDim(isRightButtonDisabled)
            .animation(nil)
    }
    
    var bottomLikeButton: some View {
        Button {
            viewModel.likeButtonAction()
        } label: {
            HStack {
                if appManager.isExternalLink {
                    Text(L10n.Drawer.Bottom.link)
                        .font(.system(size: 14, weight: .bold))
                    Image(Asset.link.name)
                        .frame(width: 15, height: 25)
                } else {
                    Image(Asset.record.name)
                        .frame(width: 25, height: 25)
                Text(appManager.allVideoData.count > 0 && appManager.isValidVideoCounter ? "\(appManager.allVideoData[appManager.videoCategory.rawValue].categoryData[appManager.videoCounter].videoLike.count)" : "0")
                        .font(.system(size: 14, weight: .semibold))
                }
            }
                .onAppear {
                    appManager.checkIfMessageExists()
                }
        }
            .buttonStyle(DrawerBottomButtonStyle())
            .padding(-5)
            .background( Color(red: 48/255, green: 95/255, blue: 146/255))
            .cornerRadius(10)
            .animation(nil)
    }
    
    var bottomCommentButton: some View {
        Button {
            appManager.commentButtonPressed.toggle()
        } label: {
            HStack {
                Image(Asset.thrComment.name)
                        .renderingMode(.template)
                        .foregroundColor(appManager.isExternalLink ? .clear : .white)
                        .frame(width: 25, height: 25)

                Text(appManager.isExternalLink ? "" : appManager.allVideoData.count > 0 && appManager.isValidVideoCounter ? "\((appManager.allVideoData[appManager.videoCategory.rawValue].categoryData[appManager.videoCounter].messageData.count - 1).roundedWithAbbreviations)" : "0")
                    .font(.system(size: 14, weight: .semibold))
            }
        }
            .buttonStyle(DrawerBottomButtonStyle())
            .padding(-5)
            .background(Color(red: 48/255, green: 95/255, blue: 146/255))
            .cornerRadius(10)
    }

    var resetButtonThetre: some View {
        HStack {
            Button {
                viewModel.reset()
            } label: {
                HStack(alignment: .center, spacing: 6) {
                    Image(appManager.showTheatreMode ? Asset.pdModeBack.name : Asset.pdModeBack.name )

                    Text(videoManager.url != nil ? L10n.Drawer.Bottom.reset : appManager.showTheatreMode ? L10n.Drawer.Bottom.pdMode : L10n.Drawer.Bottom.thrMode)
                        .font(.system(size: 14, weight: .bold))
                }
            }
                .buttonStyle(DrawerBottomButtonStyle())
                .disableAndDim(videoManager.isPlaying || exportManager.isExporting)
                .animation(nil)
                .frame(width: UIScreen.main.bounds.width * 0.275)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.white, lineWidth: 1)
                        .padding(.all, 0.5)
                        .disableAndDim(videoManager.isPlaying || exportManager.isExporting)
                )

            Spacer()
        }
    }
}
