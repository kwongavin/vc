import SwiftUI

struct SelectedSampleView: View {
    @StateObject private var recordingManager = RecordingManager.shared
    @StateObject private var sampleManager = SampleManager.shared
    @StateObject private var appManager = AppManager.shared
    @StateObject private var videoManager = VideoManager.shared

    @State var shouldAnimate = false
    @State var pulseAddIconTimer = Timer.publish(every: 2, on: .main, in: .common).autoconnect()
    @State var shouldAddIconPulse = false
    @State var tapId = 0

    var animation: Animation? {
        if appManager.shouldAnimateDrawer {
            return .easeInOut(duration: 0.2)
        } else if shouldAnimate {
            return .linear(duration: 0.15)
        } else {
            return nil
        }
    }

    private let addIconWidth: CGFloat = 15
    private let viewModel = SelectedSampleViewViewModel()

    init(index: Int) {
        viewModel.index = index
    }

    var body: some View {
        ZStack(alignment: .topLeading) {
            Group {
                if let id = viewModel.sample?.id {
                    ZStack {
                        Image(id.isEmpty ? "sample" : id)
                            .resizable()

                        Text(viewModel.sample?.shortName ?? "")
                            .font(.system(size: 13, weight: .light, design: .monospaced))
                            .foregroundColor(.white)
                    }
                } else {
                    Color(.uiTableViewBackgroundDarkBlue)
                }
            }
                .cornerRadius(16)
                .scaleEffect(viewModel.isTouchedDown ? 1.1 : 1)
                .opacity(viewModel.isTouchedDown ? 0.7 : 1)
                .animation(animation)
            
            gestureView

            if !recordingManager.isCountingDown && !recordingManager.isRecording {
                // "Hack" to make the scaling animation properly centered
                ZStack(alignment: .topLeading) {
                    sampleAddButton
                    Color(white: 0, opacity: 0)
                }
                    .scaleEffect(viewModel.isTouchedDown ? 1.1 : 1)
                    .animation(animation)
            }
        }
            .onAppear {
                pulseAddIconTimer = Timer.publish(every: 2, on: .main, in: .common).autoconnect()
            }
            .onReceive(pulseAddIconTimer) { _ in
                handlePulseAddIconTimer()
            }
    }

    var gestureView: some View {
        GestureView { gesture in
            gestureHandler(gesture: gesture)
        }
    }

    var sampleAddButton: some View {
        Button {
            viewModel.sampleAddButtonAction()
        } label: {
            ZStack {
                VectorImage(name: Asset.sampleAdd.name)
                    .scaleEffect(shouldAddIconPulse ? 1 : (addIconWidth / 44))
                    .opacity(shouldAddIconPulse ? 0 : 0.3)
                    .frame(width: 44, height: 44)

                Image(Asset.sampleAdd.name)
                    .background(
                        // "Hack" to make the tappable area bigger
                        Color(white: 0, opacity: 0.0001)
                            .frame(width: 44, height: 44)
                    )
                    .frame(width: 44, height: 44)
            }
                .animation(shouldAddIconPulse ? .default : nil)
        }
    }

    private func handlePulseAddIconTimer() {
        guard videoManager.url != nil,
              sampleManager.selectedSamples.compactMap({ $0 }).isEmpty,
              !appManager.shouldShowResetAlert,
              !sampleManager.shouldShowSampleOverrideAlert,
              !recordingManager.shouldShowRecordAlert else {
            return
        }

        shouldAddIconPulse = false

        withAnimation(.linear(duration: 0.3)) {
            shouldAddIconPulse = true
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            shouldAddIconPulse = false
        }
    }

    private func startAnimation() {
        let tapId = self.tapId

        shouldAnimate = true

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
            guard tapId == self.tapId else { return }

            shouldAnimate = false
        }
    }

    private func gestureHandler(gesture: UILongPressGestureRecognizer) {
        let sample = viewModel.sample
        let index = viewModel.index

        if sample == nil && recordingManager.isRecording { return }

        switch gesture.state {
        case .began:
            if sample != nil {
                SoundManager.shared.playSound(sampleIndex: index)

                if recordingManager.isRecording || recordingManager.isCountingDown {
                    let recording = Recording(start: videoManager.currentTime)
                    recordingManager.recordedSamples[index].append(recording)
                }
            }

            tapId += 1

            sampleManager.isTouchedDown[index] = true

            startAnimation()
        case .ended, .cancelled, .failed:
            SoundManager.shared.stopSound(sampleIndex: index)

            if sample != nil {
                if recordingManager.isRecording,
                   let recording = recordingManager.recordedSamples[index].last {
                    recording.end = videoManager.currentTime
                }
            } else if gesture.state == .ended {
                viewModel.showSampleSelection()
            }

            sampleManager.isTouchedDown[index] = false

            startAnimation()
        default: break
        }
    }
}
