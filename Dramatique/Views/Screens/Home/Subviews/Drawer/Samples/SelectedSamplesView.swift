import SwiftUI

struct SelectedSamplesView: View {
    @StateObject private var recordingManager = RecordingManager.shared
    @StateObject private var sampleManager = SampleManager.shared
    @StateObject private var appManager = AppManager.shared
    @StateObject private var videoManager = VideoManager.shared

    var sampleSize: CGFloat {
        let sampleCount = CGFloat(sampleManager.selectedSamples.count)

        return max(0, (appManager.widthWithMargin(12) - sampleSpacing * (sampleCount - 1)) / sampleCount)
    }

    private let sampleSpacing: CGFloat = 12

    var body: some View {
        HStack(spacing: sampleSpacing) {
            ForEach(0 ..< sampleManager.selectedSamples.count, id: \.self) { index in
                SelectedSampleView(index: index)
                    .frame(width: sampleSize, height: sampleSize)
            }
        }
            .sheet(isPresented: $sampleManager.shouldShowSampleSelection) {
                CategoriesView()
            }
            .alert(isPresented: $sampleManager.shouldShowSampleOverrideAlert) {
                Alert(
                    title: Text(L10n.Sample.OverrideAlert.title),
                    primaryButton: .destructive(Text(L10n.Sample.OverrideAlert.yes)) {
                        guard let sampleIndex = sampleManager.selectedSampleIndex else {
                            return
                        }

                        recordingManager.recordedSamples[sampleIndex].removeAll()
                        videoManager.load()

                        sampleManager.shouldShowSampleSelection = true
                    },
                    secondaryButton: .cancel(Text(L10n.Sample.OverrideAlert.no))
                )
            }
    }
}
