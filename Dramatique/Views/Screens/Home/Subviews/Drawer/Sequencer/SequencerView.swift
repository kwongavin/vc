import SwiftUI

struct SequencerView: View {
    @StateObject private var recordingManager = RecordingManager.shared
    @StateObject private var appManager = AppManager.shared
    @StateObject private var videoManager = VideoManager.shared

    var dragGesture: some Gesture {
        DragGesture(minimumDistance: 0)
            .onChanged { value in
                if recordingManager.isCountingDown && recordingManager.isRecording { return }

                let newPosition = min(sequencerWidth, max(0, value.location.x)) / sequencerWidth

                NotificationCenter.default.post(name: NotificationName.changeVideoCurrentTime.name, object: nil, userInfo: [UserInfoConstants.currentTime: CGFloat(videoManager.duration) * newPosition])
            }
    }

    var scrollBarColor: UIColor {
        if recordingManager.isCountingDown || recordingManager.isRecording {
            return .init(red: 1, green: 0.776, blue: 0.000, alpha: 1)
        }

        return .white
    }

    var sequencerWidth: CGFloat {
        appManager.widthWithMargin(12)
    }

    var sequencerHeight: CGFloat {
        UIScreen.main.bounds.height > 780 ? 68 : 30
    }

    let scrollBarWidth: CGFloat = 3

    var scrollBarHeight: CGFloat {
        sequencerHeight + (UIScreen.main.bounds.height > 780 ? 10 : 8)
    }

    var scrollBarOffset: CGFloat {
        if videoManager.duration == 0 {
            return -scrollBarWidth / 2
        }

        let currentTime = CGFloat(videoManager.currentTime)
        let duration = CGFloat(videoManager.duration)

        return currentTime / duration * sequencerWidth - scrollBarWidth / 2
    }

    var body: some View {
        ZStack(alignment: .leading) {
            Color(.uiTableViewBackgroundDarkBlue)
                .frame(width: sequencerWidth, height: sequencerHeight)
                .cornerRadius(UIScreen.main.bounds.height > 780 ? 16 : 10)

            SequencerTracksView(sequencerWidth: sequencerWidth)

            Color(scrollBarColor)
                .frame(width: scrollBarWidth, height: scrollBarHeight)
                .cornerRadius(scrollBarWidth / 2)
                .transformEffect(.init(translationX: scrollBarOffset, y: 0))
        }
            .gesture(dragGesture)
            .disableAndDimIfNoCurrentVideo(videoManager: videoManager)
    }
}
