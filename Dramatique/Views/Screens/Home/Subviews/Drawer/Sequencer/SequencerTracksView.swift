import SwiftUI

struct SequencerTracksView: View {
    @StateObject private var recordingManager = RecordingManager.shared
    @StateObject private var appManager = AppManager.shared
    @StateObject private var videoManager = VideoManager.shared

    let sequencerWidth: CGFloat

    private var noteHeight: CGFloat {
        UIScreen.main.bounds.height > 780 ? 12 : 6
    }

    private var verticalSpacing: CGFloat {
        UIScreen.main.bounds.height > 780 ? 6 : 3
    }

    var body: some View {
        VStack(alignment: .leading, spacing: verticalSpacing) {
            ForEach(0 ..< recordingManager.recordedSamples.count, id: \.self) { sampleIndex in
                sampleRow(sampleIndex: sampleIndex)
                    .frame(height: noteHeight)
            }
        }
    }

    private func sampleRow(sampleIndex: Int) -> some View {
        ZStack(alignment: .leading) {
            ForEach(recordingManager.recordedSamples[sampleIndex], id: \.self) { recording in
                Color(red: 0.271, green: 0.298, blue: 0.38)
                    .frame(width: noteWidth(recording: recording))
                    .cornerRadius(noteHeight / 2)
                    .padding(.leading, noteOffset(recording: recording))
            }
        }
    }

    private func noteWidth(recording: Recording) -> CGFloat {
        if videoManager.duration == 0 { return 0 }

        let recordingStart = min(recordingManager.recordingStartTime ?? 0, videoManager.currentTime)
        let recordingRange = recordingStart ... videoManager.currentTime

        let duration: Double = {
            if recordingManager.isRecording,
               !recording.isNew,
               let recordingEnd = recording.end,
               recordingRange.overlaps(recording.start ... recordingEnd) {
                return 0
            }

            return (recording.end ?? videoManager.currentTime) - recording.start
        }()

        return CGFloat(max(0, duration) / videoManager.duration) * sequencerWidth
    }

    private func noteOffset(recording: Recording) -> CGFloat {
        if videoManager.duration == 0 { return 0 }

        let recordingStart = min(recordingManager.recordingStartTime ?? 0, videoManager.currentTime)
        let recordingRange = recordingStart ... videoManager.currentTime

        let start: Double = {
            if recordingManager.isRecording,
               !recording.isNew,
               let recordingEnd = recording.end,
               recordingRange.overlaps(recording.start ... recordingEnd) {
                return 0
            }

            return recording.start
        }()

        return CGFloat(start / videoManager.duration) * sequencerWidth
    }
}
