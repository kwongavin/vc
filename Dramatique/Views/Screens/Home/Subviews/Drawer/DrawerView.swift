import SwiftUI

struct DrawerView: View {
    @StateObject private var recordingManager = RecordingManager.shared
    @StateObject private var appManager = AppManager.shared
    @StateObject private var videoManager = VideoManager.shared
    @StateObject private var keyboard = KeyboardResponder()

    var body: some View {
        VStack {
            if appManager.shouldShowRenderSettings {
                Spacer()
            }

            VStack(spacing: 0) {
                Group {
                    if appManager.shouldShowRenderSettings {
                        HStack(spacing: 0) {
                            Spacer(minLength: 12)
                            renderSettingsViewWithSpacing
                            Spacer(minLength: 12)
                        }
                    } else {
                        HStack(spacing: 0) {
                            Spacer(minLength: 12)
                            if appManager.showTheatreMode {
                                normalTheatreView
                            } else {
                                normalView
                            }
                            Spacer(minLength: 12)
                        }
                    }
                }

                DrawerBottomView()
            }
                .padding(.bottom, appManager.safeAreaInsets.bottom + 12)
                .background(appManager.showTheatreMode ? Color(.backgroundDarkBlueShadeThree) :  Color(.backgroundDarkBlue))
                .cornerRadius(16, corners: [.topLeft, .topRight])

                .offset(y: keyboard.currentHeight > 0 ? 330 - keyboard.currentHeight : 0)
                .animation(.easeInOut(duration: 0.2))
            }
                .edgesIgnoringSafeArea(.all)
    }

    var normalView: some View {
        VStack(spacing: 0) {
            Spacer()
                .frame(height: UIScreen.main.bounds.height > 568 ? 24 : 16)
// ready
            SelectedSamplesView()
                .disableAndDimIfNoCurrentVideo(videoManager: videoManager)

            Spacer()
                .frame(height: 21)

            SequencerView()

            Spacer()
        }
    }

    var normalTheatreView: some View {
        VStack(spacing: 0) {
            if UIScreen.main.bounds.height == 812.0 && keyboard.currentHeight > 0 {

            } else {
                Spacer()
                    .frame(height: UIScreen.main.bounds.height > 568 ? 24 : 16)
            }

            if appManager.commentButtonPressed {
                VStack {
                    CommentView()
                    Spacer()
                }
            } else {
                appManager.updateSubCategories ?  SelectedCategoryView() :  SelectedCategoryView()
                Spacer()
                    .frame(height: 21)
                SequencerView()
                Spacer()
            }
        }
    }

    var renderSettingsViewWithSpacing: some View {
        VStack(spacing: 0) {
            Spacer()
                .frame(height: UIScreen.main.bounds.height > 568 ? 24 : 16)

            RenderSettingsView()

            Spacer()
                .frame(height: 16)
        }
    }
}
