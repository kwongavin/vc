import Combine
import SwiftUI

struct SettingsView: View {
    @StateObject private var appManager = AppManager.shared

    var body: some View {
        NavigationView {
            List {
                HStack(spacing: 0) {
                    Toggle("", isOn: $appManager.isSoloModeOn)
                        .toggleStyle(
                            ColoredToggleStyle(
                                label: L10n.Settings.soloMode,
                                onColor: Color(.backgroundGreenShapeTwo),
                                offColor: .init(white: 0.75),
                                thumbColor: .white
                            )
                        )
                        .onReceive(Just(appManager.isSoloModeOn)) { value in
                            VideoManager.shared.load()
                        }
                }
                    .listRowBackground(Color(.uiTableViewBackgroundDarkBlue))
                    .buttonStyle(PlainButtonStyle())
            }
                .navigationBarTitle(L10n.Settings.title)
        }
            .preferredColorScheme(.dark)
    }
}
