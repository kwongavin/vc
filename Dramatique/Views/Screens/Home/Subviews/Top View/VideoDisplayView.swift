import SwiftUI
import AVKit

struct VideoDisplayView: View {
    @StateObject private var appManager = AppManager.shared
    @StateObject private var videoManager = VideoManager.shared
    @State private var shouldShowImagePicker = false
    @State private var showVideoPlayer = false

    private let placeholderIconWidth: CGFloat = 36

    var body: some View {
        ZStack {
            Button {
                videoPickerViewOnClick()
            } label: {
                ZStack {
                    ActivityIndicator(isAnimating: .constant(true), style: .medium, color: .white ?? .clear) // .white)

                    VideoPlayerView(gravity: .resizeAspectFill)
                        .opacity(shouldShowImagePicker ? 0 : 1)
                    Image(Asset.watermark.name)
                        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topTrailing)
                        .offset(x: -10, y: 7)
                        .opacity(appManager.showTheatreMode && videoManager.url != nil ? 0.1 : 0)
                }
            }
                .buttonStyle(VideoDisplayButtonStyle())
                .frame(width: appManager.widthWithMargin(12), height: appManager.widthWithMargin(12) / 16 * 9)
                .background(appManager.showTheatreMode ? Color(.lightBackgroundDarkBlueShapeTwo) : Color(.backgroundGreenShapeTwo))
                .cornerRadius(16)
                .sheet(isPresented: $shouldShowImagePicker) {
                    VideoPickerController(sourceType: .photoLibrary) { url in
                        videoManager.url = url
                        videoManager.load()
                    }
                        .edgesIgnoringSafeArea(.all)
                }
                .compatibleFullScreen(isPresented: $showVideoPlayer, content: {
                    if let url = videoManager.url  {
                        let time = CMTime(seconds: videoManager.currentTime, preferredTimescale: CMTimeScale(1.0))
                        AVPlayerView(videoURL: url, time: time)
                            .edgesIgnoringSafeArea(.all)
                            .onAppear(perform: {
                                appManager.isFullPreviewEnabled = true
                            })
                            .onDisappear {
                                appManager.isFullPreviewEnabled = false
                                let value = UIInterfaceOrientation.portrait.rawValue
                                UIDevice.current.setValue(value, forKey: "orientation")
                            }
                    }
                })
        }
    }
    
    // MARK: - Functions

    private func videoPickerViewOnClick() {
        if appManager.commentButtonPressed {
            appManager.commentButtonPressed = false
        }

        if videoManager.url == nil {
            if !appManager.showTheatreMode {
                shouldShowImagePicker = true
            }
        } else {
            if appManager.showTheatreMode {
                showVideoPlayer = true

                if videoManager.isPlaying {
                    videoManager.togglePlayback()
                }
            }
        }
    }
}
