import AVFoundation
import SwiftUI

final class VideoPlayerView: UIViewRepresentable {
    typealias UIViewType = VideoPlayerUIView
 
    let gravity: AVLayerVideoGravity

    init(gravity: AVLayerVideoGravity) {
        self.gravity = gravity
    }

    func makeUIView(context: Context) -> VideoPlayerUIView {
        .init(gravity: gravity)
    }

    func updateUIView(_ uiView: VideoPlayerUIView, context: Context) { }
}
