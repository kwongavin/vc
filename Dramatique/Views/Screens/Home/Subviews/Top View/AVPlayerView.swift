//
//  AVPlayerView.swift
//  Dramatique
//
//  Created by Difeng Chen on 6/7/22.
//

import SwiftUI
import AVKit

struct AVPlayerView: UIViewControllerRepresentable {
    var videoURL: URL
    var time: CMTime = .zero


    private var player: AVPlayer {
        AVPlayer(url: videoURL)
    }

    func updateUIViewController(_ playerController: AVPlayerViewController, context: Context) {
        playerController.player = player
        playerController.player?.seek(to: time, completionHandler: { isGiven in
            print(isGiven)
        })
        playerController.modalPresentationStyle = .fullScreen
        playerController.player?.play()
    }
    


    func makeUIViewController(context: Context) -> AVPlayerViewController {
        return AVPlayerViewController()
    }
}
