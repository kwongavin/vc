//
//  MessageForwardAndBackwardButton.swift
//  Dramatique
//
//  Created by Deepak Verma on 24/04/22.
//

import SwiftUI

struct MessageForwardAndBackwardButton: View {
    @StateObject private var appManager = AppManager.shared
    @StateObject private var videoManager = VideoManager.shared

    let isForward: Bool

    var isDisabled: Bool {
         videoManager.url == nil
    }

    var disableRight : Bool {
        appManager.messageCounter == 0
    }

    var disableLeft : Bool {
        if appManager.allVideoData.count > 0 {
            if appManager.isValidVideoCounter {
                return  appManager.messageCounter > appManager.allVideoData[appManager.videoCategory.rawValue].categoryData[appManager.videoCounter].messageData.count - 2
            } else {
                return false
            }
        } else {
           return true
        }
    }

    let size: CGFloat

    var body: some View {
        Button {
            buttonAction()
        } label: {
            Image(isForward ? Asset.undo.name : Asset.redo.name)
        }
            .buttonStyle(ControlButtonStyle(size: size, showTheatreMode: appManager.showTheatreMode))
            .disableAndDim(isForward ? disableRight : disableLeft)
    }

    private func buttonAction() {
        appManager.messageCounter += isForward ? -1 : 1
        appManager.checkIfMessageExists()
    }
}
