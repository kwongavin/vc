//
//  MessageBoardViewTheatre.swift
//  Dramatique
//
//  Created by Deepak Verma on 24/04/22.
//

import SwiftUI

struct MessageBoardViewTheatre: View {
    @StateObject private var appManager = AppManager.shared

    private let viewModel = MessageBoardViewTheatreViewModel()

    var title: String {
        return viewModel.firebaseMessages
    }

    var fontSize: CGFloat {
        if appManager.isChangingSampleVolume && appManager.isChangingOriginalVolume,
           UIScreen.main.bounds.height <= 780 {
            return 12
        }
        return 14
    }
  
    var body: some View {
        ZStack {
            messageView
            deleteMessageButtonView
        }
    }

    var messageView : some View {
        Text(title)
            .font(.system(size: fontSize, weight: .medium, design: .rounded))
            .lineLimit(
                appManager.isChangingSampleVolume && appManager.isChangingOriginalVolume ? 1 : nil
            )
            .multilineTextAlignment(.center)
            .foregroundColor(.white)
            .frame(width: appManager.widthWithMargin(12), height: UIScreen.main.bounds.height > 780 ? 63 : 43)
            .cornerRadius(16)
            .minimumScaleFactor(0.5)
            .padding(.leading, 10)
    }
    
    var deleteMessageButtonView : some View {
        ZStack {
            HStack {
                Spacer()

                if appManager.isMessageIsMine {
                    Button {
                        appManager.deleteComment()
                        appManager.checkIfMessageExists()
                    } label: {
                        Image(Asset.delete.name)
                    }
                        .padding(.trailing,5)
                }
            }
        }
    }
}
