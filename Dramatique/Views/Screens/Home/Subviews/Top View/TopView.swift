import SwiftUI

struct TopView: View {
    @StateObject private var appManager = AppManager.shared
    @StateObject private var recordingManager = RecordingManager.shared

    var body: some View {
        VStack(spacing: 12) {
            if appManager.showTheatreMode {
                ScrollView {
                    PullToRefreshControl(coordinateSpaceName: "pullToRefresh") {
                        appManager.pullToRefresh()
                    }

                    VideoDisplayView()
                    MessageBoardViewTheatre()
                    ControlsView()

                    Spacer()
                        .frame(height: 20)
                }
                    .coordinateSpace(name: "pullToRefresh")
                    .padding(.top, -7)
                    .edgesIgnoringSafeArea(.all)
                    .fixedSize(horizontal: true, vertical: true)
                    .disabled(appManager.isChangingOriginalVolume)
            } else {
                VideoDisplayView()
                MessageBoardView()
                ControlsView()

                Spacer()
                    .frame(height: 8)
            }
        }
            .padding(.top, appManager.safeAreaInsets.top + 12)
            .alert(isPresented: $recordingManager.shouldShowRecordAlert) {
                Alert(
                    title: Text(L10n.Record.OverrideAlert.title),
                    primaryButton: .default(Text(L10n.Record.OverrideAlert.yes)) {
                        recordingManager.toggleRecording()
                    },
                    secondaryButton: .cancel(Text(L10n.Record.OverrideAlert.no))
                )
            }
    }
}
