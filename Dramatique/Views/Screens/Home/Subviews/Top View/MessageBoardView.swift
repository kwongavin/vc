import SwiftUI
import FirebaseDatabase


struct MessageBoardView: View {
    @StateObject private var recordingManager = RecordingManager.shared
    @StateObject private var appManager = AppManager.shared
    @StateObject private var videoManager = VideoManager.shared
    @StateObject private var undoManager = UndoManager.shared
    @StateObject private var sampleManager = SampleManager.shared
    @StateObject private var exportManager = ExportManager.shared
    @StateObject private var messageBoardViewModel = MessageBoardViewModel()

    @State private var timer = Timer.publish(every: 11, on: .main, in: .common).autoconnect()

    var body: some View {
        Text(messageBoardViewModel.title)
            .font(.system(size: messageBoardViewModel.fontSize, weight: .medium, design: .rounded))
                .lineLimit(
                    appManager.isChangingSampleVolume && appManager.isChangingOriginalVolume ? 1 : nil
                )
                .multilineTextAlignment(.center)
                .foregroundColor(.white)
                .frame(
                    width: appManager.widthWithMargin(12),
                    height: UIScreen.main.bounds.height > 780 ? 56 : 36
                )
                .cornerRadius(16)
                .onReceive(timer) { _ in
                    if videoManager.url != nil {
                        timer.upstream.connect().cancel()
                    }

                    messageBoardViewModel.onTimerReceive()
                }
    }
}
