import AVFoundation
import UIKit

final class VideoPlayerUIView: UIView {
    let recordingHelper = RecordingHelper.shared
    let sampleHelper = SampleHelper.shared
    let settings = Settings.shared
    let soundHelper = SoundHelper.shared
    let videoHelper = VideoHelper.shared

    let gravity: AVLayerVideoGravity?

    var currentTimeDisplayLink: CADisplayLink?

    override class var layerClass: AnyClass {
        AVPlayerLayer.self
    }

    var playerLayer: AVPlayerLayer {
        layer as! AVPlayerLayer
    }

    var player: AVPlayer? {
        get {
            playerLayer.player
        }

        set {
            playerLayer.player = newValue
        }
    }

    required init?(coder aDecoder: NSCoder) {
        gravity = nil

        super.init(coder: aDecoder)
    }

    init(gravity: AVLayerVideoGravity) {
        self.gravity = gravity

        super.init(frame: .zero)

        translatesAutoresizingMaskIntoConstraints = false

        setUp()
    }

    deinit {
        teardown()
    }

    private func setUp() {
        loadVideo()

        setUpNotfications()

        currentTimeDisplayLink = .init(target: self, selector: #selector(updateCurrentTime))
        currentTimeDisplayLink?.add(to: .current, forMode: .default)
    }

    fileprivate func setUpNotfications() {
        let selectorsAndNotifNames: [(Selector, Notification.Name)] = [
            (
                #selector(loadVideo),
                NotifName.loadVideo.name
            ),
            (
                #selector(togglePlayback),
                NotifName.toggleVideoPlayback.name
            ),
            (
                #selector(play),
                NotifName.playVideo.name
            ),
            (
                #selector(pause),
                NotifName.pauseVideo.name
            ),
            (
                #selector(itemDidPlayToEndTime(_:)),
                .AVPlayerItemDidPlayToEndTime
            ),
            (
                #selector(itemFailedToPlayToEndTime(_:)),
                .AVPlayerItemFailedToPlayToEndTime
            ),
            (
                #selector(changeCurrentTime(_:)),
                NotifName.changeVideoCurrentTime.name
            ),
            (
                #selector(seekToStart),
                NotifName.seekToStartOfVideo.name
<<<<<<< HEAD
=======
            ),
            (
                #selector(teardown),
                NotifName.reset.name
>>>>>>> main
            )
        ]

        for (selector, notifName) in selectorsAndNotifNames {
            NotificationCenter.default.addObserver(
                self,
                selector: selector,
                name: notifName,
                object: nil
            )
        }
    }

    @objc fileprivate func loadVideo() {
        guard let url = videoHelper.url else {
            return
        }

        let asset = AVURLAsset(
            url: url,
            options: [AVURLAssetPreferPreciseDurationAndTimingKey: true]
        )

        let player: AVPlayer = {
            if let composition = sampleHelper.mixComposition {
                return .init(playerItem: .init(asset: composition))
            }

            return .init(playerItem: .init(asset: asset))
        }()

        self.player = player

        player.currentItem?.addObserver(
            self,
            forKeyPath: "status",
            options: [.old, .new],
            context: nil
        )
    }

    @objc fileprivate func teardown() {
        pause()

        NotificationCenter.default.removeObserver(self)

        currentTimeDisplayLink?.invalidate()
        currentTimeDisplayLink = nil

        player?.currentItem?.removeObserver(self, forKeyPath: "status")
        player = nil
    }

    @objc fileprivate func togglePlayback() {
        guard let player = player else {
            return
        }

        if player.timeControlStatus == .playing {
            pause()
        } else {
            let currentItem = player.currentItem

            if currentItem?.currentTime() == currentItem?.duration {
                seekToStart()
            }

            play()
        }
    }

    override func observeValue(
        forKeyPath keyPath: String?,
        of object: Any?,
        change: [NSKeyValueChangeKey: Any]?,
        context: UnsafeMutableRawPointer?
    ) {
        if keyPath == "status",
           let status = player?.currentItem?.status {
            switch status {
            case .readyToPlay:
                if let duration = player?.currentItem?.duration,
                   !duration.isIndefinite {
                    videoHelper.duration = CMTimeGetSeconds(duration)
                }
            case .failed: teardown()
            default: break
            }
        }
    }

    @objc fileprivate func play() {
        player?.play()
    }

    @objc fileprivate func pause() {
        player?.pause()
    }

    @objc fileprivate func changeCurrentTime(_ notification: Notification) {
        guard let currentTime = notification.userInfo?[UserInfoConstants.currentTime] as? CGFloat else {
            return
        }

        player?.seek(
            to: CMTime(
                seconds: min(videoHelper.duration, Double(currentTime)),
                preferredTimescale: 1000
            ),
            toleranceBefore: .zero,
            toleranceAfter: .zero
        )

        print(min(Double(CMTimeGetSeconds(player?.currentItem?.asset.duration ?? .zero)), Double(currentTime)))
        print(videoHelper.duration)
        print("---------")
    }

    @objc fileprivate func seekToStart() {
        player?.currentItem?.seek(to: .zero, completionHandler: nil)
    }

    @objc fileprivate func itemDidPlayToEndTime(_ notification: Notification) {
        guard let playerItem = notification.object as? AVPlayerItem,
              player?.currentItem == playerItem else {
            return
        }

        if recordingHelper.isRecording {
            recordingHelper.toggleRecording()
        } else {
            videoHelper.togglePlayback(isVideoPlaybackHandled: true)
        }
    }

    @objc fileprivate func itemFailedToPlayToEndTime(_ notification: Notification) {
        guard let playerItem = notification.object as? AVPlayerItem,
              player?.currentItem == playerItem else {
            return
        }

        teardown()
    }

    @objc fileprivate func updateCurrentTime() {
        // We cannot update the sequencer scroll bar while an alert is being displayed,
        // since it blocks the user interaction of the alert (iOS bug)
        guard videoHelper.url != nil,
              !recordingHelper.shouldShowRecordAlert,
              !settings.shouldShowResetAlert,
              !sampleHelper.shouldShowSampleOverrideAlert,
              let currentTime = player?.currentItem?.currentTime() else {
            return
        }

        videoHelper.currentTime = CMTimeGetSeconds(currentTime)
    }
}
