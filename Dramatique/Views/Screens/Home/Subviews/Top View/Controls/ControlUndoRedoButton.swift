import SwiftUI

struct ControlUndoRedoButton: View {
    @StateObject private var appManager = AppManager.shared
    @StateObject private var undoManager = UndoManager.shared

    private let viewModel = ControlUndoRedoButtonViewModel()
    let size: CGFloat

    init(isUndo: Bool, size: CGFloat) {
        viewModel.isUndo = isUndo

        self.size = size
    }

    var body: some View {
        Button {
            viewModel.isUndo ? undoManager.undo() : undoManager.redo()
        } label: {
            Image(viewModel.isUndo ? Asset.undo.name : Asset.redo.name)
        }
            .buttonStyle(ControlButtonStyle(size: size, showTheatreMode: appManager.showTheatreMode))
            .disableAndDim(viewModel.isDisabled)
    }
}
