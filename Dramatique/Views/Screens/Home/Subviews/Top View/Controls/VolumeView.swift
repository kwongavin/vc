import SwiftUI

struct VolumeView: View {
    @StateObject private var appManager = AppManager.shared
    @StateObject private var videoManager = VideoManager.shared
    @State private var isTouchedDown = false
    @State private var shouldAnimate = true

    let isSampleVolume: Bool
    let width: CGFloat

    var height: CGFloat {
        if isTouchedDown {
            return UIScreen.main.bounds.height > 780 ? 121 : 92
        }

        return width
    }

    var topPadding: CGFloat {
        width - height
    }

    var volumeLevelHeight: CGFloat {
        if isSampleVolume {
            return CGFloat(appManager.sampleVolume) * height
        }

        return CGFloat(appManager.originalVolume) * height
    }

    var body: some View {
        ZStack(alignment: .bottom) {
            if videoManager.url != nil && !appManager.shouldShowRenderSettings {

                appManager.showTheatreMode ? Color(.backgroundDarkBlueShadeTwo) : Color(.backgroundGreenShapeThree)
            }

            if appManager.showTheatreMode {
                Color(.lightBackgroundDarkBlueShapeThree)
                    .frame(height: volumeLevelHeight)
            } else {
                Color(.backgroundGreenShapeTwo)
                    .frame(height: volumeLevelHeight)
            }

            Image(isSampleVolume ? Asset.sampleVolume.name : Asset.originalVolume.name)
                .frame(width: width, height: width)

            GestureView { gesture in
                gestureCallback(gesture: gesture)
            } panGestureCallback: { gesture, location in
                gestureCallback(gesture: gesture, location: location)
            }
        }
            .frame(width: width, height: height)
            .cornerRadius(UIScreen.main.bounds.height > 568 ? 16 : 10)
            .padding(.top, topPadding)
            .animation((shouldAnimate && !appManager.isPreparing) ? .easeInOut(duration: 0.2) : nil)
    }

    func gestureCallback(gesture: UIGestureRecognizer, location: CGPoint? = nil) {
        switch gesture.state {
        case .began:
            isTouchedDown = true

            if isSampleVolume {
                appManager.isChangingSampleVolume = true
            } else {
                appManager.isChangingOriginalVolume = true
            }
        case .changed:
            guard let location = location else { return }

            shouldAnimate = false

            let newVolume = Double(min(height, max(0, height * 2 - location.y + topPadding)) / height)

            if isSampleVolume {
                appManager.sampleVolume = newVolume
                SoundManager.shared.changeVolume(volume: Float(newVolume))

                NotificationCenter.default.post(name: NotificationName.changeSampleVolume.name, object: nil, userInfo: [UserInfoConstants.volume: Float(newVolume)])
            } else {
                appManager.originalVolume = newVolume

                NotificationCenter.default.post(name: NotificationName.changeOriginalVolume.name, object: nil, userInfo: [UserInfoConstants.volume: Float(newVolume)])
            }
        case .ended, .cancelled, .failed:
            isTouchedDown = false
            shouldAnimate = true

            if isSampleVolume {
                appManager.isChangingSampleVolume = false
            } else {
                appManager.isChangingOriginalVolume = false
            }
        default: break
        }
    }
}
