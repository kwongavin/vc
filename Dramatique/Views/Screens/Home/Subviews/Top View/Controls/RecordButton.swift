import Combine
import SwiftUI

struct RecordButton: View {
    @StateObject private var recordingManager = RecordingManager.shared
    @StateObject private var sampleManager = SampleManager.shared
    @StateObject private var appManager = AppManager.shared
    @StateObject private var videoManager = VideoManager.shared

    @State private var pulseTimer = Timer.publish(every: 2, on: .main, in: .common).autoconnect()
    @State private var shouldPulse = false

    var recordImageName: String {
        if recordingManager.isRecording || recordingManager.isCountingDown {
            return Asset.stop.name
        }

        return Asset.record.name
    }

    var shouldShowPulseAnimation: Bool {
        if !isDisabled,
              videoManager.url != nil,
              !recordingManager.isRecording,
              !appManager.shouldShowResetAlert,
              !sampleManager.shouldShowSampleOverrideAlert,
              !recordingManager.shouldShowRecordAlert,
              !recordingManager.isCountingDown {
            return recordingManager.recordedSamples.flatMap({ $0 }).isEmpty
        }

        return false
    }

    var isDisabled: Bool {
        let areNoSelectedSamples = sampleManager.selectedSamples.compactMap { $0 }.isEmpty
        let shouldShowRenderSettings = appManager.shouldShowRenderSettings
        let isVideoPlaying = videoManager.isPlaying
        let isPlayableButNoSamples = areNoSelectedSamples && videoManager.url != nil && !shouldShowRenderSettings

        return isPlayableButNoSamples || isVideoPlaying
    }

    let size: CGFloat

    var iconWidth: CGFloat = 21

    var body: some View {
        Button {
            buttonAction()
        } label: {
            ZStack {
                if shouldShowPulseAnimation {
                    VectorImage(name: Asset.record.name)
                        .scaleEffect(shouldPulse ? 1 : (1 / 3))
                        .opacity(shouldPulse ? 0 : 0.3)
                        .frame(width: iconWidth * 3, height: iconWidth * 3)
                }

                Image(recordImageName)
            }
        }
            .buttonStyle(ControlButtonStyle(size: size, showTheatreMode: appManager.showTheatreMode))
            .disableAndDim(isDisabled)
            .onAppear() {
                pulseTimer = Timer.publish(every: 2, on: .main, in: .common).autoconnect()
            }
            .onReceive(pulseTimer) { _ in
                guard shouldShowPulseAnimation else {
                    return
                }

                withAnimation(.linear(duration: 0.3)) {
                    shouldPulse = true
                }

                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    shouldPulse = false
                }
            }
    }

    private func buttonAction() {
        if !recordingManager.isRecording,
           !recordingManager.isCountingDown,
           !recordingManager.recordedSamples.flatMap({ $0 }).isEmpty {
            recordingManager.shouldShowRecordAlert = true
        } else {
            recordingManager.toggleRecording()
        }
    }
}
