import SwiftUI

struct ControlsView: View {
    @StateObject private var recordingManager = RecordingManager.shared
    @StateObject private var appManager = AppManager.shared
    @StateObject private var videoManager = VideoManager.shared

    var controlSize: CGFloat {
        let controlCount: CGFloat = 6

        return max(0, (appManager.widthWithMargin(12) - controlSpacing * (controlCount - 1)) / controlCount)
    }

    private var controlSpacing: CGFloat {
        UIScreen.main.bounds.height > 568 ? 12 : 6
    }

    var body: some View {
        HStack(alignment: .bottom, spacing: controlSpacing) {
            if appManager.showTheatreMode {
                CommentButton(size: controlSize)
                    .disableAndDim(appManager.isExternalLink)
                LikeButtonTheatre(size: controlSize)
                    .disableAndDim(appManager.isExternalLink)
            } else {
                VolumeView(isSampleVolume: true, width: controlSize)
                RecordButton(size: controlSize)
            }

            Button {
                videoManager.togglePlayback()
            } label: {
                Image(videoManager.isPlaying ? Asset.pause.name : Asset.play.name)
            }
                .buttonStyle(ControlButtonStyle(size: controlSize, showTheatreMode: appManager.showTheatreMode))
                .disableAndDimIfCountdownOrRecording(recordingManager: recordingManager)

            if appManager.showTheatreMode {
                MessageForwardAndBackwardButton(isForward: true, size: controlSize).disableAndDim(appManager.isExternalLink)
                MessageForwardAndBackwardButton(isForward: false, size: controlSize).disableAndDim(appManager.isExternalLink)
            } else {
                ControlUndoRedoButton(isUndo: true, size: controlSize)
                ControlUndoRedoButton(isUndo: false, size: controlSize)
            }

            VolumeView(isSampleVolume: false, width: controlSize)
        }
            .frame(height: controlSize)
            .disableAndDim(videoManager.url == nil || appManager.shouldShowRenderSettings)
    }
}
