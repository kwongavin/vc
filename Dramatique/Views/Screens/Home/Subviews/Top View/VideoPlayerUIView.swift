import AVFoundation
import UIKit

final class VideoPlayerUIView: UIView {
    private let compositionHelper = CompositionManager.shared
    private let fileHelper = FileHelper.shared
    private let recordingManager = RecordingManager.shared
    private let sampleManager = SampleManager.shared
    private let appManager = AppManager.shared
    private let soundManager = SoundManager.shared
    private let videoManager = VideoManager.shared
    private let gravity: AVLayerVideoGravity?

    private var videoPlayerTimeObserver: Any?
    private var samplePlayer: AVPlayer?
    private var shouldIgnoreOriginalVolumeRequests = false
    private var originalVolumeWhileIgnored: Float?

    override class var layerClass: AnyClass {
        AVPlayerLayer.self
    }

    var playerLayer: AVPlayerLayer {
        layer as! AVPlayerLayer
    }

    var videoPlayer: AVPlayer? {
        get {
            playerLayer.player
        }

        set {
            playerLayer.player = newValue
        }
    }

    required init?(coder aDecoder: NSCoder) {
        gravity = nil

        super.init(coder: aDecoder)
    }

    init(gravity: AVLayerVideoGravity) {
        self.gravity = gravity

        super.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false

        DispatchQueue.main.async { [weak self] in
            self?.setUpAsync()
        }
    }

    deinit {
        pause()

        NotificationCenter.default.removeObserver(self)

        videoPlayer?.currentItem?.removeObserver(self, forKeyPath: "status")
        videoPlayer = nil

        samplePlayer = nil
    }

    private func setUpAsync() {
        DispatchQueue.main.async {
            self.setUpNotfications()
            self.loadVideo()
        }
    }

    private func setUpNotfications() {
        let selectorsAndNotificationNames: [(Selector, Notification.Name)] = [
            (#selector(loadVideo), NotificationName.loadVideo.name),
            (#selector(togglePlayback), NotificationName.toggleVideoPlayback.name),
            (#selector(play), NotificationName.playVideo.name),
            (#selector(pause), NotificationName.pauseVideo.name),
            (#selector(itemDidPlayToEndTime(_:)), .AVPlayerItemDidPlayToEndTime),
            (#selector(itemFailedToPlayToEndTime(_:)), .AVPlayerItemFailedToPlayToEndTime),
            (#selector(changeCurrentTime(_:)), NotificationName.changeVideoCurrentTime.name),
            (#selector(seekToStart), NotificationName.seekToStartOfVideo.name),
            (#selector(loadVideo), NotificationName.reset.name),
            (#selector(changeOriginalVolume(_:)), NotificationName.changeOriginalVolume.name),
            (#selector(changeSampleVolume(_:)), NotificationName.changeSampleVolume.name),
            (#selector(ignoreOriginalVolumeRequests), NotificationName.ignoreOriginalVolumeRequests.name),
            (#selector(stopIgnoringOriginalVolumeRequests), NotificationName.stopIgnoringOriginalVolumeRequests.name),
            (#selector(appDidEnterBackground), UIScene.willDeactivateNotification),
            (#selector(appWillEnterForeground), UIApplication.willEnterForegroundNotification)
        ]

        for (selector, notificationName) in selectorsAndNotificationNames {
            NotificationCenter.default.addObserver(self, selector: selector, name: notificationName, object: nil)
        }
    }

    @objc private func loadVideo() {
        let shouldPlayOnboarding = videoManager.url == nil
        
        var url: URL?

        if UserDefaults.standard.value(forKey: "onboardingCompleted") as? Bool ?? false || UserDefaults.standard.value(forKey: "nextButtonClickInSlides") as? Bool ?? false {
            url = videoManager.url ?? Bundle.main.url(forResource: appManager.showTheatreMode ? "onboardingTheatre" : "onboarding", withExtension: "mp4")
        }

        guard let url = url else { return }

        let asset = AVURLAsset(url: url, options: [AVURLAssetPreferPreciseDurationAndTimingKey: true])

        if let videoPlayerTimeObserver = videoPlayerTimeObserver {
            videoPlayer?.removeTimeObserver(videoPlayerTimeObserver)
        }

        let videoPlayer: AVPlayer = {
            if let composition = compositionHelper.originalVideoComposition {
                return .init(playerItem: .init(asset: composition))
            }

            return AVPlayer(playerItem: .init(asset: asset))
        }()

        videoPlayerTimeObserver = videoPlayer.addPeriodicTimeObserver(forInterval: .init(value: 1, timescale: 10), queue: .main) { [weak self] time in
            guard self?.videoManager.url != nil else { return }

            self?.videoManager.currentTime = time.seconds
        }

        self.videoPlayer = videoPlayer
        videoPlayer.volume = Float(appManager.originalVolume)

        if let startTime = recordingManager.recordingStartTime {
            let time = CMTime(seconds: startTime, preferredTimescale: .init(soundManager.bitrate))

            videoPlayer.seek(to: time, toleranceBefore: .zero, toleranceAfter: .zero)
        } else if let endTime = recordingManager.recordingEndTime {
            let time = CMTime(seconds: endTime, preferredTimescale: .init(soundManager.bitrate))

            videoPlayer.seek(to: time, toleranceBefore: .zero, toleranceAfter: .zero)
        }

        if let samplesComposition = compositionHelper.samplesComposition {
            let samplePlayerItem = AVPlayerItem(asset: samplesComposition)
            samplePlayerItem.audioMix = compositionHelper.audioMix

            samplePlayer = .init(playerItem: samplePlayerItem)
            samplePlayer?.volume = Float(appManager.sampleVolume)
        } else {
            samplePlayer = nil
        }

        videoPlayer.currentItem?.addObserver(self, forKeyPath: "status", options: [.old, .new], context: nil)

        appManager.checkIfMessageExists()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) { [weak self] in
            self?.appManager.checkIfLikeExists()
        }

        if shouldPlayOnboarding {
            play()
        }
    }

    @objc private func togglePlayback() {
        guard let videoPlayer = videoPlayer else { return }

        switch videoPlayer.timeControlStatus {
        case .playing:
            pause()

        default:
            let currentItem = videoPlayer.currentItem

            if currentItem?.currentTime() == currentItem?.duration {
                seekToStart()
            }

            play()
        }
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "status", let status = videoPlayer?.currentItem?.status {
            switch status {
            case .readyToPlay:
                if let duration = videoPlayer?.currentItem?.duration, !duration.isIndefinite {
                    videoManager.duration = duration.seconds
                }
            default: break
            }
        }
    }

    @objc private func play() {
        videoPlayer?.play()
        samplePlayer?.play()
    }

    @objc private func pause() {
        videoPlayer?.pause()
        samplePlayer?.pause()
    }

    @objc private func changeCurrentTime(_ notification: Notification) {
        guard let currentTime = notification.userInfo?[UserInfoConstants.currentTime] as? CGFloat else { return }

        [videoPlayer, samplePlayer].forEach {
            let time = CMTime(seconds: min(videoManager.duration, Double(currentTime)), preferredTimescale: .init(soundManager.bitrate))

            $0?.seek(to: time, toleranceBefore: .zero, toleranceAfter: .zero)
        }
    }

    @objc private func seekToStart() {
        [videoPlayer, samplePlayer].forEach {
            $0?.seek(to: .zero, toleranceBefore: .zero, toleranceAfter: .zero)
        }
    }

    @objc private func itemDidPlayToEndTime(_ notification: Notification) {
        guard let playerItem = notification.object as? AVPlayerItem,
              videoPlayer?.currentItem == playerItem else { return }

        if recordingManager.isRecording {
            soundManager.stopAllSounds()

            recordingManager.toggleRecording()
        } else if videoManager.url != nil {
            soundManager.stopAllSounds()
            
            videoManager.togglePlayback(isVideoPlaybackHandled: true)
            samplePlayer?.pause()
        } else {
            seekToStart()
            play()
        }
    }

    @objc private func itemFailedToPlayToEndTime(_ notification: Notification) {
        print("Video failed to play to end time")
    }

    @objc private func changeOriginalVolume(_ notification: Notification) {
        guard let volume = notification.userInfo?[UserInfoConstants.volume] as? Float else { return }

        if shouldIgnoreOriginalVolumeRequests {
            originalVolumeWhileIgnored = volume
        } else {
            videoPlayer?.volume = volume
        }
    }

    @objc private func ignoreOriginalVolumeRequests() {
        shouldIgnoreOriginalVolumeRequests = true
    }

    @objc private func stopIgnoringOriginalVolumeRequests() {
        shouldIgnoreOriginalVolumeRequests = false

        if let originalVolumeWhileIgnored = originalVolumeWhileIgnored {
            appManager.originalVolume = Double(originalVolumeWhileIgnored)
            self.originalVolumeWhileIgnored = nil
        }
    }

    @objc private func appDidEnterBackground() {
        if videoManager.isPlaying {
            videoManager.togglePlayback()
        }
    }

    @objc private func appWillEnterForeground() {
        if videoManager.url == nil {
            videoPlayer?.play()
        }
    }

    @objc private func changeSampleVolume(_ notification: Notification) {
        guard let volume = notification.userInfo?[UserInfoConstants.volume] as? Float else { return }

        samplePlayer?.volume = volume
    }
}
