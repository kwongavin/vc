import AVFoundation

extension AVPlayer {
    // github.com/eharrison/AVPlayerFade/blob/master/AVPlayerFade/AVPlayerExtensions.swift
    func setVolume(
        _ targetVolume: Float,
        duration: Float,
        timer: inout Timer?,
        completion: (() -> Void)? = nil
    ) {
        timer?.invalidate()
        timer = nil

        let prevVolume = volume

        // there's no point in continuing if target volume is the same as initial
        guard targetVolume != prevVolume else {
            return
        }

        let interval: Float = 0.01
        let range = targetVolume - prevVolume

        // based on the range, the interval and duration, we calculate how big is the step
        // we need to take in order to reach the target in the given duration
        let step = range * interval / duration

        // internal function whether the target has been reached or not
        var reachedTarget: Bool {
            // volume passed max/min
            guard volume >= 0, volume <= 1 else {
                volume = targetVolume

                return true
            }

            // checks whether the volume is going forward or backward and compare current volume to target
            if targetVolume > prevVolume {
                return volume >= targetVolume
            }

            return volume <= targetVolume
        }

        timer = .scheduledTimer(
            withTimeInterval: Double(interval),
            repeats: true
        ) { [weak self] timer in
            guard let self = self else {
                return
            }

            if !reachedTarget {
                self.volume += step
            } else {
                timer.invalidate()

                completion?()
            }
        }
    }
}
