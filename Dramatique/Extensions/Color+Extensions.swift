//
//  Color+Extensions.swift
//  Dramatique
//
//  Created by Difeng Chen on 6/4/22.
//

import SwiftUI

extension Color {
    init(_ appColor: UIColor.AppColor) {
        self = Color(UIColor(appColor: appColor) ?? .clear)
    }
}
