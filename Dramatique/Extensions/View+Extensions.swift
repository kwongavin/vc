import SwiftUI

extension View {
    func disableAndDimIfCountdownOrRecording(recordingManager: RecordingManager) -> some View {
        disableAndDim(recordingManager.isRecording || recordingManager.isCountingDown)
    }

    func disableAndDimIfNoCurrentVideo(videoManager: VideoManager) -> some View {
        disableAndDim(videoManager.url == nil)
    }

    func disableAndDim(_ expression: Bool) -> some View {
        opacity(expression ? 0.3 : 1).disabled(expression)
    }

    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape(RoundedCornersShape(radius: radius, corners: corners))
    }

    func compatibleFullScreen<Content: View>(isPresented: Binding<Bool>, @ViewBuilder content: @escaping () -> Content) -> some View {
        self.modifier(FullScreenModifier(isPresented: isPresented, builder: content))
    }
}
