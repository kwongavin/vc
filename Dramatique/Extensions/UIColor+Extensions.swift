//
//  UIColor+Extensions.swift
//  Dramatique
//
//  Created by Difeng Chen on 6/4/22.
//

import UIKit

extension UIColor {
    enum AppColor: String {
        case backgroundDarkBlue
        case lightBackgroundDarkBlue
        case uiTableViewBackgroundDarkBlue
        case backgroundGreen
        case backgroundGreenShapeTwo
        case appYellow
        case lightBackgroundDarkBlueShapeTwo
        case backgroundDarkBlueShadeTwo
        case backgroundGreenShapeThree
        case lightBackgroundDarkBlueShapeThree
        case backgroundDarkBlueShadeThree
        case backgroundDarkBlueShadeFour
    }

    convenience init?(appColor: AppColor) {
        self.init(named: appColor.rawValue)
    }
}

extension UIColor {
    static let backgroundDarkBlue = UIColor(appColor: .backgroundDarkBlue)
    static let lightBackgroundDarkBlue = UIColor(appColor: .lightBackgroundDarkBlue)
    static let uiTableViewBackgroundDarkBlue = UIColor(appColor: .uiTableViewBackgroundDarkBlue)
    static let backgroundGreen = UIColor(appColor: .backgroundGreen)
    static let appYellow = UIColor(appColor: .appYellow)
    static let lightBackgroundDarkBlueShapeTwo = UIColor(appColor: .lightBackgroundDarkBlueShapeTwo)
    static let backgroundGreenShapeTwo = UIColor(appColor: .backgroundGreenShapeTwo)
    static let backgroundDarkBlueShadeTwo = UIColor(appColor: .backgroundDarkBlueShadeTwo)
    static let backgroundGreenShapeThree = UIColor(appColor: .backgroundGreenShapeThree)
    static let lightBackgroundDarkBlueShapeThree = UIColor(appColor: .lightBackgroundDarkBlueShapeThree)
    static let backgroundDarkBlueShadeThree = UIColor(appColor: .backgroundDarkBlueShadeThree)
    static let backgroundDarkBlueShadeFour = UIColor(appColor: .backgroundDarkBlueShadeFour)
}
