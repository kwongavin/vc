//
//  Date+Extensions.swift
//  Dramatique
//
//  Created by Difeng Chen on 6/6/22.
//

import Foundation

extension Date {
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
