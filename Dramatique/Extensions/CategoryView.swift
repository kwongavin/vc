//
//  CategoryView.swift
//  Dramatique
//
//  Created by Difeng Chen on 6/12/22.
//

import SwiftUI

struct CategoryView: View {
    let sampleSize: CGFloat
    let selectedCategory: SelectedVideoCategory
    let title: String

    @Binding var isCategorySelected: Bool
    @StateObject private var appManager = AppManager.shared

    var body: some View {
        HStack {
            Button {
                isCategorySelected.toggle()
                appManager.updateSubCategories.toggle()
                if isCategorySelected {
                    appManager.videoCategorySelected = selectedCategory
                }
            } label: {
                Text(title)
                    .foregroundColor(isCategorySelected ? .white : .gray)
                    .font(.system(size: 13, weight: .light, design: .monospaced))
                    .frame(width: sampleSize, height: sampleSize)
                    .onTapGesture {
                        appManager.categorySelected1 = false
                        appManager.categorySelected2 = false
                        appManager.categorySelected3 = false

                        if title == "Art" {
                            appManager.categorySelected1.toggle()
                        }else if title == "Laugh" {
                            appManager.categorySelected2.toggle()

                        }else {
                            appManager.categorySelected3.toggle()
                        }
                    }
             
            }
        }
            .frame(width: sampleSize, height: sampleSize)
            .background(Color(.uiTableViewBackgroundDarkBlue).opacity(0.5))
            .cornerRadius(10)
    }
}
